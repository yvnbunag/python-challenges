from src.__modules__ import challenge, objectListInput, levelPrint

class N148_minimumAndMaximumFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/python-basic-exercise-148.php"
    self.challengeDescription = "Write a Python function to find the maximum "\
      "and minimum numbers from a sequence of numbers. Note: Do not use " \
      "built-in functions."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    minimum = 0
    maximum = 0
    numberList = objectListInput(int)

    minimum = numberList[0]
    for number in numberList:
      if maximum < number:
        maximum = number
        continue
      elif minimum > number:
        minimum = number
    print(f'The maximum number in the set is: {maximum}')
    print(f'The minimum number in the set is: {minimum}')

if __name__ == "__main__":
  main = N148_minimumAndMaximumFinder()
  main.execute()