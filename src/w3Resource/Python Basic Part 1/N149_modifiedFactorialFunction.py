from src.__modules__ import challenge

class N149_modifiedFactorialFunction(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/python-basic-exercise-149.php"
    self.challengeDescription = "Write a Python function that takes a positive "\
      "integer and returns the sum of the cube of all the positive integers " \
      "smaller than the specified number."
    self.timeComplexity = "Time Complexity: O(n!) - Factorial Time"

  def execute(self) -> None:
    isPositive = False

    while not isPositive:
      userInput = input("Enter a positive integer: ")
      try:
        userInput = int(userInput)
        if userInput < 0:
          raise ValueError
        isPositive = True
      except ValueError:
        print("Value entered is not a positive integer\n") 
    
    factor = self.modifiedFactorial(userInput-1)
    print(
      f'Sum of the cube of positive numbers smaller than {userInput} is: ' \
      f'{factor}'
    )

  def modifiedFactorial(self, value):
    if value == 1:
      return 1
    return (value ** 3) + self.modifiedFactorial(value - 1)

if __name__ == "__main__":
  main = N149_modifiedFactorialFunction()
  main.execute()