from src.__modules__ import challenge, objectListInput, levelPrint

class N150_distinctOddProductFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/python-basic-exercise-150.php"
    self.challengeDescription = "Write a Python function to find a distinct "\
      "pair of numbers whose product is odd from a sequence of integer values."
    self.timeComplexity = "Time Complexity: O(n^2) - Quadratic Time"
        
  def execute(self) -> None:
    integerList = objectListInput(int)
    oddCombination = []
    for outerInt in integerList:
      for innerInt in integerList:
        if outerInt == innerInt:
          continue
        if (outerInt * innerInt) & 1:
          oddCombination.append([outerInt,innerInt])
    
    if len(oddCombination):
      print("Combinations in the sequence that results in odd product are:")

      sortedCombination = []
      for val in oddCombination:
          val.sort()
          if val not in sortedCombination:
            sortedCombination.append(val)

      for sVal in sortedCombination:
          levelPrint(1, f'{sVal[0]} * {sVal[1]} = {sVal[0] * sVal[1]}')

    else:
      print("No combination in the sequence results in an odd product")

if __name__ == "__main__":
  main = N150_distinctOddProductFinder()
  main.execute()
