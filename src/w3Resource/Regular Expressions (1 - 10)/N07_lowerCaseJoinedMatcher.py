from src.__modules__ import challenge
import re

class N07_lowerCaseJoinedMatcher(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-7.php"
    self.challengeDescription = "Write a Python program to find sequences of "\
      "lowercase letters joined with a underscore."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "aab_cbbbc",
      "aab_Abbbc",
      "Aaab_abbbc"
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'^[a-z]+_[a-z]+$'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N07_lowerCaseJoinedMatcher()
  main.execute()