from src.__modules__ import challenge
import re

class N06_sequenceRequirementMatcher_2(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-6.php"
    self.challengeDescription = "Write a Python program to find sequences of "\
      "lowercase letters joined with a underscore."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "ab",
      "aab_Abbbc",
      "aabbbbbc",
      "aabbbccc"
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'ab{2,3}[^b]'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N06_sequenceRequirementMatcher_2()
  main.execute()