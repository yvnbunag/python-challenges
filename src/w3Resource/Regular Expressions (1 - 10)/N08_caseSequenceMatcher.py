from src.__modules__ import challenge
import re

class N08_caseSequenceMatcher(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-8.php"
    self.challengeDescription = "Write a Python program to find sequences of "\
      "one upper case letter followed by lower case letters."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "aab_cbbbc",
      "aab_Abbbc",
      "Aaab_abbbc",
      "Aaababbbc",
      "AaababbbcA",
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'^[A-Z][a-z]+$'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N08_caseSequenceMatcher()
  main.execute()