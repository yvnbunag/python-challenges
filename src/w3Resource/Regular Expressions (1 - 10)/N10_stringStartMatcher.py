from src.__modules__ import challenge
import re

class N10_stringStartMatcher(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-10.php"
    self.challengeDescription = "Write a Python program that matches a word "\
      "at the beginning of a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "The quick brown fox jumps over the lazy dog.",
      " The quick brown fox jumps over the lazy dog."
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'^[A-Za-z]+\b'
    return re.match(regex, string)
  
if __name__ == "__main__":
  main = N10_stringStartMatcher()
  main.execute()