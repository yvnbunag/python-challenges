from src.__modules__ import challenge
import re

class N04_sequenceOptionalMatcher(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-4.php"
    self.challengeDescription = "Write a Python program that matches a string "\
      "that has an a followed by zero or one 'b'."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "ab",
      "abc",
      "abbc",
      "aac",
      "aaabbcc"
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'ab?([^b]|\b)'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N04_sequenceOptionalMatcher()
  main.execute()