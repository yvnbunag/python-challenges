from src.__modules__ import challenge
import re

class N01_stringContentValidator(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-1.php"
    self.challengeDescription = "Write a Python program to check that a "\
      "string contains only a certain set of characters (in this case a-z, A-Z and 0-9)."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "ABCDEFabcdef123450",
      "ABCDEFabcdef123450_",
      "*&%@#!}{",
    ]
    for string in strings:
      print(
        string,
        "=>",
       bool(self.containsCharacterSet(string))
      )

  def containsCharacterSet(self, string: str):
    regex = r'^[^\W_]+$'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N01_stringContentValidator()
  main.execute()