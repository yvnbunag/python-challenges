from src.__modules__ import challenge
import re

class N03_sequenceRequiredMatcher(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-3.php"
    self.challengeDescription = "Write a Python program that matches a string "\
      "that has an a followed by one or more b's."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "ab",
      "abc",
      "abbc",
      "aacccc",
      "aaabbbbbbbbcc",
      "aaaaaaaaacccccccccc"
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'ab+'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N03_sequenceRequiredMatcher()
  main.execute()