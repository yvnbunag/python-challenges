from src.__modules__ import challenge
import re

class N02_sequenceOptionalOrMoreMatcher(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-2.php"
    self.challengeDescription = "Write a Python program that matches a string "\
      "that has an a followed by zero or more b's."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "ac",
      "abc",
      "abbc",
      "bbccc"
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'ab*'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N02_sequenceOptionalOrMoreMatcher()
  main.execute()