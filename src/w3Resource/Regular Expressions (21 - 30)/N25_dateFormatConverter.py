from src.__modules__ import challenge
import re

class N25_dateFormatConverter(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-25.php"
    self.challengeDescription = "Write a Python program to convert a date of "\
      "yyyy-mm-dd format to dd-mm-yyyy format."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "2026-01-02"
    print(string, "=>", self.changeDateFormat(string))

  def changeDateFormat(self, string: str) -> str:
    regex = r'(\d{4,})-(\d{1,2})-(\d{1,2})'
    return re.sub(regex, r"\3-\2-\1", string)
  
if __name__ == "__main__":
  main = N25_dateFormatConverter()
  main.execute()