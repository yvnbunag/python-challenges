from src.__modules__ import challenge
import re

class N30_wordAbbreviation(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-30.php"
    self.challengeDescription = "Write a Python program to abbreviate 'Road' "\
      "as 'Rd.' in a given string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "21 Ramkrishna Road"
    string2 = "Road road"
    print(string, "=>", self.abbreviateRoad(string))
    print(string2, "=>", self.abbreviateRoad(string2))

  def abbreviateRoad(self, string: str) -> str:
    regex = r'[Rr]oad$'
    return re.sub(regex, "Rd.", string)
  
if __name__ == "__main__":
  main = N30_wordAbbreviation()
  main.execute()