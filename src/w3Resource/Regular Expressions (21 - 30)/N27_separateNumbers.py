from src.__modules__ import challenge
import re

class N27_separateNumbers(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-27.php"
    self.challengeDescription = "Write a Python program to separate and print "\
      "the numbers of a given string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "Ten 10, Twenty 20, Thirty 30, Forty and a half 40.5,"\
      "Non float number 50..62, Double dot 70.8.90"
    print("String value:", string)
    print("Numbers in the string:")
    for word in self.separateNumbers(string):
      print(word)

  def separateNumbers(self, string: str) -> list:
    regex = r'\d+\.?\d+'
    return re.findall(regex, string)
  
if __name__ == "__main__":
  main = N27_separateNumbers()
  main.execute()