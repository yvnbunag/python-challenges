from src.__modules__ import challenge
import re

class N24_dateFromURLExtractor(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-24.php"
    self.challengeDescription = "Write a Python program to extract year, "\
      "month and date from an url."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "https://www.washingtonpost.com/news/football-insider/wp/"\
      "2016/09/02/odell-beckhams-fame-rests-on-one-stupid-little-ball"\
      "-josh-norman-tells-author/"
    print(string, "=>", self.extractDate(string))

  def extractDate(self, string: str) -> dict:
    regex = r'(\d{4,})[-/](\d{1,2})[-/](\d{1,2})'
    return list(map(
      lambda match: {
        "year": match[0],
        "month":match[1],
        "date": match[2]
      },
      re.findall(regex, string)
    ))
  
if __name__ == "__main__":
  main = N24_dateFromURLExtractor()
  main.execute()