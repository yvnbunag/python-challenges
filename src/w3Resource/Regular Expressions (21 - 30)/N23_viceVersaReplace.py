from src.__modules__ import challenge
import re

class N23_viceVersaReplace(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-23.php"
    self.challengeDescription = "Write a Python program to replace "\
      "whitespaces with an underscore and vice versa."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "Python Exercises_Vice Versa_Replacement"
    print(string, "=>", self.replaceViceVersa(string))

  def replaceViceVersa(self, string: str) -> str:
    regex = r'[_\s]'
    return re.sub(
      regex,
      lambda match: "_" if match.group() == " " else " ",
      string
      )
  
if __name__ == "__main__":
  main = N23_viceVersaReplace()
  main.execute()