from src.__modules__ import challenge
import re

class N21_occurrenceFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-21.php"
    self.challengeDescription = "Write a Python program to find the "\
      "occurrence and position of the substrings within a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "Python exercises, PHP exercises, C# exercises"
    pattern = "exercises"
    print("Source string:", string)
    for match in self.findOccurrences(string, pattern):
      print(f'Found "{match}"')

  def findOccurrences(self, string: str, pattern: str) -> list:
    regex = f'{pattern}'
    return re.findall(regex, string)
  
if __name__ == "__main__":
  main = N21_occurrenceFinder()
  main.execute()