from src.__modules__ import challenge
import re

class N29_numbersAndPositionFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-29.php"
    self.challengeDescription = "Write a Python program to separate and print "\
      "the numbers and their position of a given string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "The following example creates an ArrayList with a capacity of "\
      "50 elements. 4 elements are then added to the ArrayList and the "\
      "ArrayList is trimmed accordingly."
    print("String value:", string)
    print("Numbers and position in string:")
    for data in self.findNumbersAndPosition(string):
      print(" Value:", string[data["start"]:data["end"]])
      print("  Start index:", data["start"])
      print("  End index:", data["end"])

  def findNumbersAndPosition(self, string: str) -> list:
    regex = r'\d+'
    return list(map(
      lambda x: {
        "value": x.group(),
        "start": x.start(),
        "end": x.end()
      },
      re.finditer(regex, string)
    ))
  
if __name__ == "__main__":
  main = N29_numbersAndPositionFinder()
  main.execute()