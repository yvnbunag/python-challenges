from src.__modules__ import challenge
import re

class N26_matchStartingWithCount(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-26.php"
    self.challengeDescription = "Write a Python program to match if two words "\
      "from a list of words starting with letter 'P'."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    words = ["Python PHP", "Java JavaScript", "c c++"]
    for word in words:
      res = self.getTwoMatchesWithP(word)
      print(word, "=>", res.groups() if res else res)

  def getTwoMatchesWithP(self, string: str) -> list:
    regex = r'(P\w+)\W+(P\w+)'
    return re.match(regex, string)
  
if __name__ == "__main__":
  main = N26_matchStartingWithCount()
  main.execute()