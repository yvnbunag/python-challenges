from src.__modules__ import challenge
import re

class N28_wordsStartingWithFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-28.php"
    self.challengeDescription = "Write a Python program to find all words "\
      "starting with 'a' or 'e' in a given string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "The following example creates an ArrayList with a capacity of "\
      "50 elements. 4 elements are then added to the ArrayList and the "\
      "ArrayList is trimmed accordingly."
    print("String value:", string)
    print("Words in string starting with 'a' and 'e':")
    for word in self.findWordsStartingWithAOrE(string):
      print(word)

  def findWordsStartingWithAOrE(self, string: str) -> list:
    regex = r'\b[AaEe]\w+\b'
    return re.findall(regex, string)
  
if __name__ == "__main__":
  main = N28_wordsStartingWithFinder()
  main.execute()