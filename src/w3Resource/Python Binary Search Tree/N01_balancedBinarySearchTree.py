from src.__modules__ import challenge, Node, objectListInput

class N01_balancedBinarySearchTree(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-binary-search-tree-exercise-1.php"
    self.challengeDescription = "Write a Python program to create a Balanced "\
      "Binary Search Tree (BST) using an array (given) elements where array " \
      "elements are sorted in ascending order."
    self.timeComplexity = "O(n log n) - Quasilinear Time"

  def execute(self) -> None:
    arrayToBST = [1, 7, 2, 6, 3, 5, 4]
    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToBST = []
        break
      print("Please enter a valid answer\n")

    if arrayToBST == []:
      arrayToBST = objectListInput(int)
    print("Original array value:")
    print(arrayToBST)

    # Create tree from given list
    node = Node()
    for value in arrayToBST:
      node.insert(value)
  
    # Output tree as ascending list
    print("Sorted tree values: ")
    print(Node.getAscendingReturned(node))
    # Output tree hierarchy
    print("Current tree hierarchy: ")
    Node.nodeOrderView(node)
    print("\n")
    
    # Balance current hierarchy
    node.balanceSelf()

    # Output tree as ascending list
    print("Balanced tree values: ")
    print(Node.getAscendingReturned(node))
    # Output balanced tree hierarchy
    print("Balanced tree hierarchy: ")
    Node.nodeOrderView(node)

if __name__ == "__main__":
  main = N01_balancedBinarySearchTree()
  main.execute()