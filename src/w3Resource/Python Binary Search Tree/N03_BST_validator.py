from src.__modules__ import challenge, objectListInput, Node

class N03_BST_validator(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-binary-search-tree-exercise-3.php"
    self.challengeDescription = "Write a Python program to check whether a "\
      "given a binary tree is a valid binary search tree (BST) or not."
    self.timeComplexity = "O(n log n) - Quasilinear Time"

  def execute(self) -> None:
    validNode = Node(4)
    validNode.leftChild = Node(2)
    validNode.leftChild.leftChild = Node(1)
    validNode.leftChild.rightChild = Node(3)
    validNode.rightChild = Node(6)
    validNode.rightChild.leftChild = Node(5)
    validNode.rightChild.rightChild = Node(7)

    invalidNode = Node(4)
    invalidNode.leftChild = Node(2)
    invalidNode.leftChild.leftChild = Node(1)
    invalidNode.leftChild.rightChild = Node(3)
    invalidNode.rightChild = Node(5)
    invalidNode.rightChild.leftChild = Node(6)
    invalidNode.rightChild.rightChild = Node(7)

    print("Valid node values printed in ascending hierarchy are: ")
    print(Node.getAscendingReturned(validNode))
    print("Result of valid node run through validator: ")
    print(Node.validateBinaryTree(validNode))
    print()
    print("Invalid node values printed in ascending hierarchy are: ")
    print(Node.getAscendingReturned(invalidNode))
    print("Result of invalid node run through validator: ")
    print(Node.validateBinaryTree(invalidNode))


  def validate(node):
    pass

if __name__ == "__main__":
  main = N03_BST_validator()
  main.execute()