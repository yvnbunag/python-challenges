from src.__modules__ import challenge, Node, objectListInput

class N04_BST_nodeDeletion(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-binary-search-tree-exercise-4.php"
    self.challengeDescription = "Write a Python program to delete a node with "\
      "the given key in a given Binary search tree (BST)."
    self.timeComplexity = "O(log n) - Logarithmic Time"

  def execute(self) -> None:
    arrayToBST = [1, 15, 2, 14, 3, 13, 4, 12, 5, 11, 6, 10, 7, 9, 8]
    deleteQuery = 8
    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToBST = []
        break
      print("Please enter a valid answer\n")

    if arrayToBST == []:
      arrayToBST = objectListInput(int)
      while True:
        try:
          deleteQuery = int(input("Enter target value: "))
        except ValueError:
          print("Value to be deleted must be a valid integer.\n")
          continue
        break
    else:
      print("Array to convert to BST values are:", arrayToBST)
      print("Value to delete is: ", deleteQuery)
      print()


    node = Node()
    for value in arrayToBST:
      node.insert(value)
    
    node.balanceSelf()

    print("Values of the tree in order are: ")
    print(Node.getAscendingReturned(node))
    print()
    print("Value to be deleted found in the tree?")
    print("Yes" if node.deleteNode(deleteQuery) else "No")
    print()
    print("Values of the tree after delete operation: ")
    print(Node.getAscendingReturned(node))

if __name__ == "__main__":
  main = N04_BST_nodeDeletion()
  main.execute()