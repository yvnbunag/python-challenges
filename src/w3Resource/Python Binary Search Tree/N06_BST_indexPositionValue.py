from src.__modules__ import challenge, Node, objectListInput

class N06_BST_indexPositionValue(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-binary-search-tree-exercise-6.php"
    self.challengeDescription = "Write a Python program to find the kth "\
      "smallest element in a given a binary search tree."
    self.timeComplexity = "O(log n) - Logarithmic Time"

  def execute(self) -> None:
    arrayToBST = [1, 15, 2, 14, 3, 13, 4, 12, 5, 11, 6, 10, 7, 9, 8]
    indexValue = 8
    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToBST = []
        break
      print("Please enter a valid answer\n")

    if arrayToBST == []:
      arrayToBST = objectListInput(int)
      while True:
        try:
          indexValue = int(input("Enter target index: "))
        except ValueError:
          print("Index value must be a valid integer.\n")
          continue
        break
    else:
      print("Array to convert to BST values are:", arrayToBST)
      print("Index to get from tree is: ", indexValue)

    node = Node()
    for value in arrayToBST:
      node.insert(value)
    
    node.balanceSelf()

    print("\nTree values are: ")
    print(Node.getAscendingReturned(node))
    try:
      value = node[indexValue]
      print(f'Value at index {indexValue} is: {value}')
    except IndexError:
      print("Index out of bounds in tree")
    except ValueError:
      print("Invalid index value")

if __name__ == "__main__":
  main = N06_BST_indexPositionValue()
  main.execute()