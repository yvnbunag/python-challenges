from src.__modules__ import challenge, Node, objectListInput

class N02_BST_closestTargetValue(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-binary-search-tree-exercise-2.php"
    self.challengeDescription = "Write a Python program to find the closest "\
      "value of a given target value in a given non-empty Binary Search Tree " \
      "(BST) of unique values."
    self.timeComplexity = "O(log n) - Logarithmic Time"

  def execute(self) -> None:
    arrayToBST = [8, 5, 14, 4, 6, 8, 7, 24, 22]
    targetValue = 19
    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToBST = []
        break
      print("Please enter a valid answer\n")

    if arrayToBST == []:
      arrayToBST = objectListInput(int)
      while True:
        try:
          targetValue = int(input("Enter target value: "))
        except ValueError:
          print("Target value must be a valid integer.\n")
          continue
        break
    else:
      print("Array to convert to BST values are:", arrayToBST)
      print("Target value is: ", targetValue)

    # Create tree from given list
    node = Node()
    for value in arrayToBST:
      node.insert(value)
    
    # Balance current hierarchy
    node.balanceSelf()
    
    print("Sorted array value in tree: ")
    print(Node.getAscendingReturned(node))

    print("Closest value to target is: ")
    print(Node.getClosestTargetValue(node, targetValue))

if __name__ == "__main__":
  main = N02_BST_closestTargetValue()
  main.execute()