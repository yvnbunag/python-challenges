from src.__modules__ import challenge
import re

class N14_stringContentMatcher(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-14.php"
    self.challengeDescription = "Write a Python program to match a string that "\
      "contains only upper and lowercase letters, numbers, and underscores."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "The quick brown fox jumps over the lazy dog.",
      "Python_Exercises_1"
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'^\w+$'
    return re.match(regex, string)
  
if __name__ == "__main__":
  main = N14_stringContentMatcher()
  main.execute()