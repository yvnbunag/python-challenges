from src.__modules__ import challenge
import re

class N12_wordContainsCharacterMatcher(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-12.php"
    self.challengeDescription = "Write a Python program that matches a word "\
      "containing 'z'."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "The quick brown fox jumps over the lazy dog.",
      "Python_Exercises_1"
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'\w*z\w*'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N12_wordContainsCharacterMatcher()
  main.execute()