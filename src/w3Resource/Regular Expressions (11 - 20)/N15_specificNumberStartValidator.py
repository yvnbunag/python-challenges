from src.__modules__ import challenge
import re

class N15_specificNumberStartValidator(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-15.php"
    self.challengeDescription = "Write a Python program where a string will "\
      "start with a specific number."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    startValue = "5"
    for string in ["5-2345861", "6-2345861"]:
      print(string, "=>", bool(self.validateLeadingNumber(string, startValue)))

  def validateLeadingNumber(self, string: str, leading: str):
    regex = f'^{leading}'
    return re.match(regex, string)
  
if __name__ == "__main__":
  main = N15_specificNumberStartValidator()
  main.execute()