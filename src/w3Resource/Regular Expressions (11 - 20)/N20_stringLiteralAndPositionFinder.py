from src.__modules__ import challenge
import re

class N20_stringLiteralAndPositionFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-20.php"
    self.challengeDescription = "Write a Python program to search a literals "\
      "string in a string and also find the location within the original "\
      "string where the pattern occurs."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "The quick brown fox jumps over the lazy dog."
    pattern = "fox"
    print("Source string:", string)
    m = self.findOccurrencesAndPosition(string, pattern)
    print(
      f'Found {m.group()} in "{string}" from {m.start()} to {m.end()}'
    )

  def findOccurrencesAndPosition(self, string: str, pattern: str):
    regex = f'{pattern}'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N20_stringLiteralAndPositionFinder()
  main.execute()