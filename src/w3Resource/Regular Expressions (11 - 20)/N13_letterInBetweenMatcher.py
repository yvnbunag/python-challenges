from src.__modules__ import challenge
import re

class N13_letterInBetweenMatcher(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-13.php"
    self.challengeDescription = "Write a Python program that matches a word "\
      "containing 'z', not start or end of the word."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "The quick brown fox jumps over the lazy dog.",
      "Python_Exercises_1"
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'\Bz\B'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N13_letterInBetweenMatcher()
  main.execute()