from src.__modules__ import challenge
import re

class N11_stringEndMatcher(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-11.php"
    self.challengeDescription = "Write a Python program that matches a word "\
      "at end of string, with optional punctuation."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    strings = [
      "The quick brown fox jumps over the lazy dog.",
      "The quick brown fox jumps over the lazy dog. ",
      "The quick brown fox jumps over the lazy dog ",
      "The quick brown fox jumps over the lazy dog",
      "Thequickbrownfoxjumpsoverthelazydog!",
      "Thequickbrown16foxjumpsoverthelazydog"
    ]
    for string in strings:
      print(
        string,
        "=>",
        "Found a match!" if self.matchStringContent(string) else "Not matched!"
      )

  def matchStringContent(self, string: str):
    regex = r'\b[A-Za-z]+[.!?]?$'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N11_stringEndMatcher()
  main.execute()