from src.__modules__ import challenge
import re

class N16_ipLeadingZeroesRemover(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-16.php"
    self.challengeDescription = "Write a Python program to check for a number "\
      "at the end of a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    for string in ["216.08.094.196", "010.090.255.255"]:
      print(string, "=>", self.removeLeadingZeroes(string))

  def removeLeadingZeroes(self, string: str) -> str:
    regex = r'(\D|^)(0+)'
    return re.sub(regex, r"\1", string)
  
if __name__ == "__main__":
  main = N16_ipLeadingZeroesRemover()
  main.execute()