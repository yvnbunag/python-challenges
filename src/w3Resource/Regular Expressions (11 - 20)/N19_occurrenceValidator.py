from src.__modules__ import challenge
import re

class N19_occurrenceValidator(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-19.php"
    self.challengeDescription = "Write a Python program to search some "\
      "literals strings in a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "The quick brown fox jumps over the lazy dog."
    patterns = ["fox", "dog", "horse"]

    for pattern in patterns:
      print(
        f'Searching for {pattern} in "{string}" ->',
        "Matched" if self.searchOccurrence(string, pattern) else "Not Matched!"
      )

  def searchOccurrence(self, string: str, pattern: str):
    regex = f'{pattern}'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N19_occurrenceValidator()
  main.execute()