from src.__modules__ import challenge
import re

class N17_numberAtTheEndValidator(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-17.php"
    self.challengeDescription = "Write a Python program to check for a number "\
      "at the end of a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    for string in ["abcdef", "abcdef6", "6abcdef"]:
      print(
        string,
        "=>",
        bool(self.validateNumberAtEnd(string))
      )

  def validateNumberAtEnd(self, string: str):
    regex = r'\d$'
    return re.search(regex, string)
  
if __name__ == "__main__":
  main = N17_numberAtTheEndValidator()
  main.execute()