from src.__modules__ import challenge
import re

class N18_numberInRangeFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-18.php"
    self.challengeDescription = "Write Python program to search the numbers "\
      "(0-9) of length between 1 to 3 in a given string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "55 Exercises number 1, 12, 13, 345 and 6789 are important 88"

    print("Original string:", string)
    print("Numbers of length 1 to 3:")

    for match in self.findNumberRangeOccurrence(string):
      print(match)

  def findNumberRangeOccurrence(self, string: str) -> list:
    regex = r'\b\d{1,3}\b'
    return re.findall(regex, string)
  
if __name__ == "__main__":
  main = N18_numberInRangeFinder()
  main.execute()