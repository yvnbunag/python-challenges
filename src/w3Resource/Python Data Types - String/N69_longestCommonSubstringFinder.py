from src.__modules__ import challenge, levelPrint

class N69_longestCommonSubstringFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/string/python-data-type-string-exercise-69.php"
    self.challengeDescription = "Write a Python program to find the longest "\
      "common sub-string from two given strings."
    self.timeComplexity = "O(2^n) - Exponential Time"
    

  def execute(self) -> None:
    stringList = []
    for current in ['first', 'second']:
      validInput = False
      while not validInput:
        userInput = input(f'Enter {current} string: ')
        
        if len(userInput):
          validInput = True
          stringList.append(userInput)
          continue
        print("Please enter a valid string\n")

    substrings = []
    
  
    for iteration in range(2):
      currentSub = ""
      ctr = 1
      for char in stringList[iteration]:
        previousSub = currentSub
        currentSub += char
        
        if not currentSub in stringList[1 - iteration]:
          if (previousSub in stringList[1 - iteration] and
              previousSub != ""):
            substrings.append(previousSub)
          currentSub = char
        if (len(stringList[iteration]) == ctr and
            currentSub in stringList[1 - iteration]):
            substrings.append(currentSub)

        ctr+=1
    
    substrings = list(set(substrings))
    
    try:
      maxLength = len(max([s for s in substrings if len(s) > 1], key=len))

      print("\nLongest common substrings are: ")
      for s in [s for s in substrings if len(s) == maxLength]:
        levelPrint(1, s)
        
    except Exception as err:
      if err.__class__.__name__ == "ValueError":
        if not len(substrings):
          print("Longest common sub-string not present")

if __name__ == "__main__":
  main = N69_longestCommonSubstringFinder()
  main.execute()