from src.__modules__ import challenge, levelPrint
import re

class N74_minimumStringWindowEvaluator(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/string/python-data-type-string-exercise-74.php"
    self.challengeDescription = "Write a Python program to find the minimum "\
      "window in a given string which will contain all the characters of " \
      "another given string.\nExample 1:\nInput:\nstr1 = \" PRWSOERIUSFK \"\n" \
      "str2 = \" OSU \"\nOutput:\nMinimum window is \"OERIUS\""
    self.timeComplexity = "O(n log n) - Quasilinear Time"

  def execute(self) -> None:
    isValidInput = False
    sourceString = "PRWSOERIUSFK"
    minimumWindow = "OSU"
    yn = ""

    while not isValidInput:
      yn = input("Use example values as variables? (y/n): ")
      
      if yn.lower() == "y":
        isValidInput = True
      elif yn.lower() == "n":
        sourceString = input(
          "Enter string to extract minimum window of another string: "
        )
        minimumWindow = input(
          "Enter string window to extract: "
        )
        isValidInput = True
      else:
        print("Selection not within given range of values")

      print()

    iterationFinished = False
    matches = []
    
    # regexValue = f'([{minimumWindow}](.*?[{minimumWindow}]){{2}})'
    # Not supported? || different algorithm from javascript?

    regexValue = f'[{minimumWindow}]'
    for ctr in range(len(minimumWindow) - 1):
      regexValue += f'.*?[{minimumWindow}]'
    regexValue = f'({regexValue})'

    while not iterationFinished:
      matchedFilter = ""
      for match in matches:
        # Generate regex to exclude already matched values
        matchedFilter += f'(?!{match})'

      iterationMatch = re.findall(
        f'{matchedFilter}{regexValue}', sourceString
      )
      if len(iterationMatch):
        matches += iterationMatch
      else:
        iterationFinished = True

    if yn.lower() == "y":
        print(f'String to extract from is: {sourceString}')
        print(f'Minimum window matcher is: {minimumWindow}')
    
   
    if len(matches):
      # Iterate through the matches and filter out results that does not contain
      # all of the required characters
      filteredMatches = []
      for match in matches:
        isComplete = True
  
        for char in minimumWindow:
          if char not in match:
            isComplete = False
            break

        if isComplete:
          filteredMatches.append(match)
  
      minLength = len(min(filteredMatches, key=len))
      print("Minimum string window matches are:")

      # Iterate through the matches and filter with the shortest string length 
      # result
      for match in filteredMatches:
        if len(match) == minLength:
          levelPrint(1, match)
    else:
      print("There are no matches")

if __name__ == "__main__":
  main = N74_minimumStringWindowEvaluator()
  main.execute()