from src.__modules__ import challenge
import re
class N73_characterSetCounter(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/string/python-data-type-string-exercise-73.php"
    self.challengeDescription = "Write a Python program to count Uppercase, "\
      " Lowercase, special character and numeric values in a given string."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    evaluationSet = {
      "uppercase": "[A-Z]",
      "lowercase": "[a-z]",
      "numeric": "\d",
      "special case": "[^A-Za-z0-9]"
    }
    validInput = False
    while not validInput:
      userInput = input("Enter string to be evaluated: ")
      
      if len(userInput):
        validInput = True
        continue
      print("Please enter a valid string\n") 

    for key, val in evaluationSet.items():
      matches = re.findall(val, userInput)
      print(f'Number of {key} characters: {len(matches)}')

if __name__ == "__main__":
  main = N73_characterSetCounter()
  main.execute()