from src.__modules__ import challenge

class N02_radixConverter(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-recursion-exercise-2.php"
    self.challengeDescription = "Write a Python program to converting an integer to a "\
      "string in any base." 
    self.timeComplexity = "O(n!) - Factorial Time"

  def execute(self) -> None:
    while True:
      try:
        num = int(input("Enter integer to convert to different bases: "))
      except ValueError:
        print("Please enter a valid integer\n")
        continue
      break

    print(f'{num} in binary(base 2) is: {self.radixConverter(num, 2)}')
    print(f'{num} in octal(base 8) is: {self.radixConverter(num, 8)}')
    print(f'{num} in hex(base 16) is: {self.radixConverter(num, 16)}')
  
  def radixConverter(self, num: int, base: int) -> str:
    radixCollection = "0123456789ABCDEF"
    if num < base:
      return radixCollection[num]
    return self.radixConverter(num//base, base) + radixCollection[num % base]

if __name__ == "__main__":
  main = N02_radixConverter()
  main.execute()