from src.__modules__ import challenge

class N10_powerFunctionality(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-recursion-exercise-10.php"
    self.challengeDescription = "Write a Python program to calculate the value"\
      " of 'a' to the power 'b'."
    self.timeComplexity = "O(n!) - Factorial Time"

  def execute(self) -> None:
    numbers = []
    toInput = "base"
    while len(numbers) < 2:
      try:
        number = int(input(f'Enter {toInput} integer: '))
      except ValueError:
        print("Please enter a valid integer.\n")
        continue
      numbers.append(number)
      toInput = "power"

    print(
      f'{numbers[0]} to the power of {numbers[1]} is: {self.power(numbers)}'
    )
  
  def power(self, powerList: list) -> int:
    if powerList[1] <= 0:
      return 1
    powerList[1] -= 1
    return powerList[0] * self.power(powerList)

if __name__ == "__main__":
  main = N10_powerFunctionality()
  main.execute()