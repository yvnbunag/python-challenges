from src.__modules__ import challenge

class N06_individualIntegerSummation(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "value"
    self.challengeDescription = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-recursion-exercise-6.php"\
      "Write a Python program to get the sum of a non-negative integer.\n" \
      " Test Data:\n" \
      " sumDigits(345) -> 12\n" \
      " sumDigits(45) -> 9" 
    self.timeComplexity = "O(n!) - Factorial Time"

  def execute(self) -> None:
    while True:
      try:
        num = int(input("Enter positive integer: "))
        if num < 0:
          raise ValueError
      except ValueError:
        print("Please enter a valid positive integer\n")
        continue
      break

    print(
      f'Sum of individual integers are: {self.individualIntegerSum(num)}'
    )

  
  def individualIntegerSum(self, num) -> int:
    if not num:
      return 0
    numToStr = str(num)
    return int(numToStr[0]) + self.individualIntegerSum(numToStr[1:])


if __name__ == "__main__":
  main = N06_individualIntegerSummation()
  main.execute()