from src.__modules__ import challenge

class N04_factorial(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-recursion-exercise-4.php"
    self.challengeDescription = "Write a Python program to get the factorial "\
      "of a non-negative integer."
    self.timeComplexity = "O(n!) - Factorial Time"

  def execute(self) -> None:
    while True:
      try:
        num = int(input("Enter positive integer: "))
        if num < 0:
          raise ValueError
      except:
        print("Please enter a valid positive integer\n")
        continue
      break
    
    print(
      f'The factorial of {num} is: {self.factorial(num)}'
    )

  def factorial(self, num: int) -> int:
    if num <= 1:
      return 1
    return num * self.factorial(num - 1)

if __name__ == "__main__":
  main = N04_factorial()
  main.execute()