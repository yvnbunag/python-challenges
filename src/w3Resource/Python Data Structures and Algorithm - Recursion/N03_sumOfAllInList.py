from src.__modules__ import challenge

class N03_sumOfAllInList(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-recursion-exercise-3.php"
    self.challengeDescription = "Write a Python program of recursion list sum."
    self.timeComplexity = "O(n!) - Factorial Time"

  def execute(self) -> None:
    nested = [1, 2, [3,4],[5,6],[[1,2,3],[4,5,6]],[7,8,9],[10, 11, 12]]
    print("Nested list values are: " ,nested)
    print("Sum of all integers in the list is: ", self.recursionSum(nested))
  
  def recursionSum(self, collection: list) -> int:
    if not collection:
      return 0
    
    if isinstance(collection[0], list):
      return self.recursionSum(collection[0]) + self.recursionSum(collection[1:])
    
    return collection[0] + self.recursionSum(collection[1:])

if __name__ == "__main__":
  main = N03_sumOfAllInList()
  main.execute()