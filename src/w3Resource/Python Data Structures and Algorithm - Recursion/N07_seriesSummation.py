from src.__modules__ import challenge

class N07_seriesSummation(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-recursion-exercise-7.php"
    self.challengeDescription = "Write a Python program to calculate the sum "\
      "of the positive integers of n+(n-2)+(n-4)... (until n-x =< 0)."
    self.timeComplexity = "O(n!) - Factorial Time"

  def execute(self) -> None:
    while True:
      try:
        num = int(
          input(
            "Enter integer to undergo series summation [n+(n-2)+(n-4)... " \
            "while n-x <= 0]: "
          )
        )
      except ValueError:
        print("Please enter a valid integer\n")
        continue
      break

    print(f'Result of series summation: {self.seriesSummation(num)}')

  def seriesSummation(self, num: int) -> int:
    if num <= 0:
      return 0
    return num + self.seriesSummation(num - 2)

if __name__ == "__main__":
  main = N07_seriesSummation()
  main.execute()