from src.__modules__ import challenge, objectListInput

class N08_mergeSort(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-8.php"
    self.challengeDescription = "Write a Python program to sort a list of "\
      "elements using the merge sort algorithm."
    self.timeComplexity = "O(n log n) - Quasilinear Time"

  def execute(self) -> None:
    arrayToSort = [14,46,43,27,57,41,45,21,70]

    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToSort = []
        break
      print("Please enter a valid answer\n")

    if arrayToSort == []:
      arrayToSort = objectListInput(int)
    print("Original array value:")
    print(arrayToSort)
    print("\nArray sorted in ascending using merge sort:")
    print(self.mergeSortAscending(arrayToSort))
    print("\nArray sorted in descending using merge sort:")
    print(self.mergeSortDescending(arrayToSort))

  def mergeSortAscending(self, arr:list) -> list:
    # Maps array, with each value inserted inside another array
    arr = [[val] for val in arr]

    # Iterates through all the values, merging them in pairs of two until
    # everything is merged into a single array
    while True:
      # Displays the iteration process
      print(arr)
      
      # Stops the loop if there is only one remaining array
      arrLen = len(arr)
      if arrLen == 1:
        arr = arr[0]
        break

      # Creates a collection of the left half of the array
      # Consists of even indexed values in the array
      lArray = arr[0::2]

      # Loop through odd indexed arrays
      for rIndex, rArray in enumerate(arr[1::2]):
        # Loop through the right-array values
        for rValue in rArray:
          # Loop through left-array values of the same right-root-index
          for lIndex, lValue in enumerate(lArray[rIndex]):
            lastLeftIndex = len(lArray[rIndex]) - 1
            # Compares values in the direction of the loop, inserting right
            # value into the left array when condition is matched
            if lValue > rValue:
              lArray[rIndex].insert(lIndex, rValue)
              break
            if lastLeftIndex == lIndex:
              lArray[rIndex].insert((lIndex + 1), rValue)
              break

        # Left array is now mixed with right array values, assigning it to the 
        # main array and rerunning the loop
        arr = lArray
    return arr

  def mergeSortDescending(self, arr:list) -> list:
    # Maps array, with each value inserted inside another array
    arr = [[val] for val in arr]

    # Iterates through all the values, merging them in pairs of two until
    # everything is merged into a single array
    while True:
      # Displays the iteration process
      print(arr)
      
      # Stops the loop if there is only one remaining array
      arrLen = len(arr)
      if arrLen == 1:
        arr = arr[0]
        break

      # Creates a collection of the left half of the array
      # Consists of even indexed values in the array
      lArray = arr[0::2]

      # Loop through odd indexed arrays
      for rIndex, rArray in enumerate(arr[1::2]):
        # Loop through the right-array values
        for rValue in rArray:
          # Loop through left-array values of the same right-root-index
          for lIndex, lValue in enumerate(lArray[rIndex]):
            lastLeftIndex = len(lArray[rIndex]) - 1
            # Compares values in the direction of the loop, inserting right
            # value into the left array when condition is matched
            if lValue < rValue:
              lArray[rIndex].insert(lIndex, rValue)
              break
            if lastLeftIndex == lIndex:
              lArray[rIndex].insert((lIndex + 1), rValue)
              break

        # Left array is now mixed with right array values, assigning it to the 
        # main array and rerunning the loop
        arr = lArray
    return arr

  

if __name__ == "__main__":
  main = N08_mergeSort()
  main.execute()