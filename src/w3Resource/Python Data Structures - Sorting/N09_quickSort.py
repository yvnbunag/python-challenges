from src.__modules__ import challenge, objectListInput, customString as cs 

class N09_quickSort(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-9.php"
    self.challengeDescription = "Write a Python program to sort a list of "\
      "elements using the quick sort algorithm."
    self.timeComplexity = "O(n log n) - Quasilinear Time"

  def execute(self) -> None:
    arrayToSort = [54,26,93,17,77,31,44,55,20]

    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToSort = []
        break
      print("Please enter a valid answer\n")

    if arrayToSort == []:
      arrayToSort = objectListInput(int)
    
    arrayToSort2 = list(arrayToSort)
    print("Original array value:")
    print(arrayToSort)
    print("\nArray sorted in ascending using quick sort:")
    self.quickSortAscending(arrayToSort, start = 0, end = len(arrayToSort)-1)
    print(arrayToSort)
    print("\nArray sorted in descending using quick sort:")
    self.quickSortDescending(arrayToSort2, start = 0, end = len(arrayToSort)-1)
    print(arrayToSort2)

  def quickSortAscending(
    self,
    arr:list,
    start: int = 0,
    end: int = 0,
    display:bool = True
    ) -> None:
    """
      Quick sort pivot algorithm implements using the last index as pivot.
      Method returns none as it sorts in place, using merely reference to the 
      array that is being sorted
    """

    # Pivot is equal to the last index in the partition
    pivotIndex = hIndex = end
    lIndex = start
    # Length of the partition range
    collectionLength = end - start

    # For live sorting operation view
    if display:
      for index, value in enumerate(arr):    
        if index == pivotIndex:
          print(
            cs.colored(f'({value})', "quickSortPivot"),
            end = cs.colored("] ","quickSortPartition")
          )
        elif index == start:
          print(
            cs.colored(f'[{value}', "quickSortPartition"),
            end = " "
          )
        elif start < index < pivotIndex:
          print(
            cs.colored(value, "quickSortPartition"),
            end = " "
          )
        else:
          print(value, end = " ")
      print()

    # Iterate through the partition range. If current value in the leftmost(lowest
    # value partition) is lower that the pivot, increase the lowest value index.
    # If value is higher that the pivot, decrease the highest value index and 
    # swap values
    for ctr in range(collectionLength):
      if arr[lIndex] < arr[pivotIndex]:
        lIndex += 1
      else:
        hIndex -= 1
        arr[lIndex], arr[hIndex] = arr[hIndex], arr[lIndex]

    # Once the partitions have been determined, move the pivot to the right of
    # the lower value partition and to the left of the higher value partition,
    # if present
    while pivotIndex != lIndex:
      arr[pivotIndex], arr[pivotIndex - 1] = arr[pivotIndex - 1], arr[pivotIndex]
      pivotIndex -= 1

    # If there are still partitions, either or both to the left and right of the
    # pivot, perform recursive quick sorting on those partitions
    if lIndex > start and start != lIndex - 1:
      self.quickSortAscending(arr, start = start, end = lIndex - 1, display = display)
    if hIndex < end and hIndex + 1 != end:
      self.quickSortAscending(arr, start = hIndex + 1, end = end, display = display)

  def quickSortDescending(self, arr:list, start: int = 0, end: int = 0) -> None:
    """
      Quick sort pivot algorithm implements using the last index as pivot.
      Method returns none as it sorts in place, using merely reference to the 
      array that is being sorted
    """

    # Instantiate important variables: pivotIndex, highestIndex, lowestIndex and
    # collection length
    pivotIndex = hIndex = end
    lIndex = start
    # length of the partition range
    collectionLength = end - start

    # For live sorting operation view
    for index, value in enumerate(arr):    
      if index == pivotIndex:
        print(
          cs.colored(f'({value})', "quickSortPivot"),
          end = cs.colored("] ","quickSortPartition")
        )
      elif index == start:
        print(
          cs.colored(f'[{value}', "quickSortPartition"),
          end = " "
        )
      elif start < index < pivotIndex:
        print(
          cs.colored(value, "quickSortPartition"),
          end = " "
        )
      else:
        print(value, end = " ")
    print()

    # Iterate through the partition range. If current value in the leftmost(lowest
    # value partition) is lower that the pivot, increase the lowest value index.
    # If value is higher that the pivot, decrease the highest value index and 
    # swap values
    for ctr in range(collectionLength):
      if arr[lIndex] > arr[pivotIndex]:
        lIndex += 1
      else:
        hIndex -= 1
        arr[lIndex], arr[hIndex] = arr[hIndex], arr[lIndex]

    # Once the partitions have been determined, move the pivot to the right of
    # the lower value partition and to the left of the higher value partition,
    # if present
    while pivotIndex != lIndex:
      arr[pivotIndex], arr[pivotIndex - 1] = arr[pivotIndex - 1], arr[pivotIndex]
      pivotIndex -= 1

    # If there are still partitions, either or both to the left and right of the
    # pivot, perform recursive quick sorting on those partitions
    if lIndex > start and start != lIndex - 1:
      self.quickSortDescending(arr, start = start, end = lIndex - 1)
    if hIndex < end and hIndex + 1 != end:
      self.quickSortDescending(arr, start = hIndex + 1, end = end)

  
if __name__ == "__main__":
  main = N09_quickSort()
  main.execute()