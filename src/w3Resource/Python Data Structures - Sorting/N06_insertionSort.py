from src.__modules__ import challenge, objectListInput

class N06_insertionSort(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-6.php"
    self.challengeDescription = "Write a Python program to sort a list of "\
      "elements using the insertion sort algorithm."
    self.timeComplexity = "O(n^2) - Quadratic Time"

  def execute(self) -> None:
    arrayToSort = [14,46,43,27,57,41,45,21,70]

    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToSort = []
        break
      print("Please enter a valid answer\n")

    if arrayToSort == []:
      arrayToSort = objectListInput(int)
    print("Original array value:")
    print(arrayToSort)
    print("Array sorted in ascending using insertion sort:")
    print(self.insertionSortAscending(arrayToSort))
    print("Array sorted in descending using insertion sort:")
    print(self.insertionSortDescending(arrayToSort))

  def insertionSortAscending(self, arr: list) -> list:
    sortedArr = arr[0:1]
    sortedLen = 1
    for val in arr[1:]:
      position = sortedLen // 2
      direction = 0

      if val <= sortedArr[position]:
        direction -= 1
      else:
        direction += 1
      while True:

        if ((sortedArr[position] - val) * direction) > 0:
          sortedArr.insert(position + ((direction - 1) // -2), val)
          sortedLen += 1
          break
        elif position == 0 or position == sortedLen - 1:
          sortedArr.insert(position + ((direction + 1) // 2), val)
          sortedLen += 1
          break

        position += direction
    
    return sortedArr
  
  def insertionSortDescending(self, arr: list) -> list:
    sortedArr = arr[0:1]
    sortedLen = 1
    for val in arr[1:]:
      position = sortedLen // 2
      direction = 0

      if val >= sortedArr[position]:
        direction -= 1
      else:
        direction += 1
      while True:
        if ((sortedArr[position] - val) * direction) < 0:
          sortedArr.insert(position + ((direction - 1) // -2), val)
          sortedLen += 1
          break
        elif position == 0 or position == sortedLen - 1:
          sortedArr.insert(position + ((direction + 1) // 2), val)
          sortedLen += 1
          break

        position += direction
    
    return sortedArr
        
if __name__ == "__main__":
  main = N06_insertionSort()
  main.execute()