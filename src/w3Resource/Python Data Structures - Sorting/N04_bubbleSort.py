from src.__modules__ import challenge, objectListInput

class N04_bubbleSort(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-4.php"
    self.challengeDescription = "Write a Python program to sort a list of "\
      "elements using the bubble sort algorithm."
    self.timeComplexity = "O(n^2) - Quadratic Time"

  def execute(self) -> None:
    arrayToSort = [14,46,43,27,57,41,45,21,70]

    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToSort = []
        break
      print("Please enter a valid answer\n")

    if arrayToSort == []:
      arrayToSort = objectListInput(int)
    print("Original array value:")
    print(arrayToSort)
    print("Array sorted in ascending using bubble sort:")
    print(self.bubbleSortAscending(arrayToSort))
    print("Array sorted in descending using bubble sort:")
    print(self.bubbleSortDescending(arrayToSort))

  def bubbleSortAscending(self, arr: list) -> list:
    arrLength = len(arr)
    while True:
      isSorted = True
      for index in range(arrLength - 1):
        if arr[index + 1] < arr[index]:
          arr[index], arr[index + 1] = arr[index + 1], arr[index]
          isSorted = False
      if isSorted:
        break
    return arr

  def bubbleSortDescending(self, arr):
    arrLength = len(arr)
    while True:
      isSorted = True
      for index in range(arrLength - 1):
        if arr[index + 1] > arr[index]:
          arr[index], arr[index + 1] = arr[index + 1], arr[index]
          isSorted = False
      if isSorted:
        break
    return arr


if __name__ == "__main__":
  main = N04_bubbleSort()
  main.execute()