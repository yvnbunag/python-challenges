from src.__modules__ import challenge, objectListInput

class N08_mergeSort_2(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-8.php"
    self.challengeDescription = "Write a Python program to sort a list of "\
      "elements using the merge sort algorithm. This is a second variation " \
      "of the already completed challenge."
    self.timeComplexity = "O(n log n) - Quasilinear Time"

  def execute(self) -> None:
    arrayToSort = [14,46,43,27,57,41,45,21,70]

    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToSort = []
        break
      print("Please enter a valid answer\n")

    if arrayToSort == []:
      arrayToSort = objectListInput(int)
    print("Original array value:")
    print(arrayToSort)
    print("\nArray sorted in ascending using merge sort:")
    print(self.mergeSortAscending(arrayToSort))
    print("\nArray sorted in descending using merge sort:")
    print(self.mergeSortDescending(arrayToSort))

  def mergeSortAscending(self, arr: list) -> list:
    # Stops the recursion if arr has a single value (all values are merged)
    if len(arr) == 1:
      return arr

    # Divides the current array into left and right collections
    leftArray = arr[len(arr) // 2:]
    rightArray = arr[:len(arr) // 2] 

    # Recursively runs the left and right array collections into self
    if leftArray:
      leftArray = self.mergeSortAscending(leftArray)

    if rightArray:
      rightArray = self.mergeSortAscending(rightArray)
    
    # Loops through the values of the right array to be inserted to the left
    for rIndex, rValue in enumerate(rightArray):
      # Loops through the values in the left array for comparison and insertion
      for lIndex, lValue in enumerate(leftArray):
        # Compares values in the direction of the loop, inserting right
        # value into the left array when condition is matched
        if lValue > rValue:
          leftArray.insert(lIndex, rValue)
          break
        if lIndex == len(leftArray) - 1:
          leftArray.insert((lIndex + 1), rValue)
          break
    
    # Returns sorted list, which would be combined with another sorted list
    # until it reaches root of the recursive call
    return leftArray

  def mergeSortDescending(self, arr: list) -> list:
    # Stops the recursion if arr has a single value (all values are merged)
    if len(arr) == 1:
      return arr
  
    
    # Divides the current array into left and right collections
    leftArray = arr[len(arr) // 2:]
    rightArray = arr[:len(arr) // 2] 

    # Recursively runs the left and right array collections into self
    if leftArray:
      leftArray = self.mergeSortDescending(leftArray)

    if rightArray:
      rightArray = self.mergeSortDescending(rightArray)
    
    # Loops through the values of the right array to be inserted to the left
    for rIndex, rValue in enumerate(rightArray):
      # Loops through the values in the left array for comparison and insertion
      for lIndex, lValue in enumerate(leftArray):
        # Compares values in the direction of the loop, inserting right
        # value into the left array when condition is matched
        if lValue < rValue:
          leftArray.insert(lIndex, rValue)
          break
        if lIndex == len(leftArray) - 1:
          leftArray.insert((lIndex + 1), rValue)
          break
    
    # Returns sorted list, which would be combined with another sorted list
    # until it reaches root of the recursive call
    return leftArray

if __name__ == "__main__":
  main = N08_mergeSort_2()
  main.execute()