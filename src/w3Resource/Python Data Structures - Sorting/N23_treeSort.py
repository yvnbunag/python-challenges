from src.__modules__ import challenge, objectListInput

class N23_treeSort(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-23.php"
    self.challengeDescription = "Write a Python program to sort a list of"\
      " elements using Tree sort."
    self.timeComplexity = "O(n log n) - Quasilinear Time"

  def execute(self) -> None:
    arrayToSort = [7,1,5,2,19,14,17]

    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToSort = []
        break
      print("Please enter a valid answer\n")

    if arrayToSort == []:
      arrayToSort = objectListInput(int)

    tree = node() 
    for value in arrayToSort:
      tree.insert(value)
    
    print("Original array value:")
    print(arrayToSort)

    print(
      "Print of ascending list using standard reference appending method:"
    )
    collectionArray = []
    tree.getAscending(tree, collectionArray)
    print(collectionArray)

    print(
      "Print of descending list using standard reference appending method:"
    )
    collectionArray = []
    tree.getDescending(tree, collectionArray)
    print(collectionArray)

    print(
      "Print of ascending list using return method:"
    )
    print(tree.getAscendingReturned(tree))

    print(
      "Print of descending list using return method:"
    )
    print(tree.getDescendingReturned(tree))

class node():
  def __init__(self, value = None):
    self.value = value
    self.leftChild = None
    self.rightChild = None

  def insert(self, nextValue) -> None:
    if self.value is not None:
      if nextValue <= self.value:
        if self.leftChild is None:
          self.leftChild = node(nextValue)
        else:
          self.leftChild.insert(nextValue)
      elif nextValue > self.value:
        if self.rightChild is None:
          self.rightChild = node(nextValue)
        else:
          self.rightChild.insert(nextValue)
    else:
      self.value = nextValue
    
  def getAscending(self, rootNode, collection) -> None:
    if rootNode:
      self.getAscending(rootNode.leftChild, collection)
      collection.append(rootNode.value)
      self.getAscending(rootNode.rightChild, collection)
  
  def getDescending(self, rootNode, collection) -> None:
    if rootNode:
      self.getDescending(rootNode.rightChild, collection)
      collection.append(rootNode.value)
      self.getDescending(rootNode.leftChild, collection)

  def getAscendingReturned(self, rootNode) -> list:
    collection = []
    if rootNode:
      collection += self.getAscendingReturned(rootNode.leftChild)
      collection += [rootNode.value]
      collection += self.getAscendingReturned(rootNode.rightChild)
    return collection

  def getDescendingReturned(self, rootNode) -> list:
    collection = []
    if rootNode:
      collection += self.getDescendingReturned(rootNode.rightChild)
      collection += [rootNode.value]
      collection += self.getDescendingReturned(rootNode.leftChild)
    return collection
    
if __name__ == "__main__":
  main = N23_treeSort()
  main.execute()