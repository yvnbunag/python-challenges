from src.__modules__ import challenge, objectListInput

class N20_selectionSort(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-20.php"
    self.challengeDescription = "Write a Python program to sort a list of "\
      "elements using Selection sort."
    self.timeComplexity = "O(n^2) - Quadratic Time"

  def execute(self) -> None:
    arrayToSort = [15, 79, 25, 37, 68]

    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToSort = []
        break
      print("Please enter a valid answer\n")

    if arrayToSort == []:
      arrayToSort = objectListInput(int)
    print("Original array value:")
    print(arrayToSort)
    print("Array sorted in ascending using selection sort:")
    print(self.selectionAscendingSort(arrayToSort))
    print("Array sorted in descending using selection sort:")
    print(self.selectionDescendingSort(arrayToSort))


  def selectionAscendingSort(self, arr) -> list:
    arrayIndexList = range(len(arr))
    for oIndex in arrayIndexList[0:]:
      leastValue = arr[oIndex]
      lowerIndex = oIndex

      for iIndex in arrayIndexList[(oIndex + 1):]:
        if arr[iIndex] < leastValue:
          leastValue = arr[iIndex]
          lowerIndex = iIndex

      arr[oIndex], arr[lowerIndex] = arr[lowerIndex], arr[oIndex]

    return arr

  def selectionDescendingSort(self, arr) -> list:
    arrayIndexList = range(len(arr))
    for oIndex in arrayIndexList[0:]:
      leastValue = arr[oIndex]
      lowerIndex = oIndex

      for iIndex in arrayIndexList[(oIndex + 1):]:
        if arr[iIndex] > leastValue:
          leastValue = arr[iIndex]
          lowerIndex = iIndex

      arr[oIndex], arr[lowerIndex] = arr[lowerIndex], arr[oIndex]

    return arr


      

if __name__ == "__main__":
  main = N20_selectionSort()
  main.execute()