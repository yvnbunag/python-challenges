from src.__modules__ import challenge, objectListInput

class N63_tableRowsAndColumnsSum(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/basic/python-basic-1-exercise-63.php"
    self.challengeDescription = "Write a Python program which adds up columns "\
      "and rows of given table."
    self.timeComplexity = "O(n^2) - Quadratic Time"

  def execute(self) -> None:
    isValidAnswer = False
    useDefault = True
    tableDimension = 4
    table = [
      [25, 69, 51, 26],
      [68, 35, 29, 54],
      [54, 57, 45, 63],
      [61, 68, 47, 59]
    ]
    
    while not isValidAnswer:
      userInput = input("Use default table with a dimension of 4x4? (y/n): ")
      if userInput.lower() == "n":
        useDefault = False
        isValidAnswer = True
        continue
      elif userInput.lower() == "y":
        isValidAnswer = True
        continue
      else:
        print("Selection not within the options provided\n")

    if not useDefault:
      isValidAnswer = False
      while not isValidAnswer:
        tableDimension = input("Input number of rows/columns: ")
        try:
          tableDimension = int(tableDimension)
          if tableDimension <= 0:
            raise ZeroValueError
          isValidAnswer = True
          continue
        except:
          print("Invalid input, enter only a valid integer\n")

      table = self.tablePrompt(tableDimension)

    print("The sum of the rows and columns of the table are represented as: ")
    columnSums = [0] * tableDimension
    for row in table:
      rowSum = 0
      ctr = 0
      for column in row:
        rowSum += column
        print('{0:>5}'.format(column), end = " ")
        columnSums[ctr] += column
        ctr+=1
      print('{0:>5}'.format(rowSum))

    for column in columnSums:
      print('{0:>5}'.format(column), end = " ")
    print('{0:>5}'.format(sum(columnSums)))

  def tablePrompt(self, dimension:int) -> list:
    table = []
    print(
      "Enter values for table (columns delimited by space and rows by line break)"
    )
    for row in range(dimension):
      columnSet = objectListInput(int, dimension, dimension, "tabbed")
      table.append(columnSet)
    return table
    
if __name__ == "__main__":
  main = N63_tableRowsAndColumnsSum()
  main.execute()