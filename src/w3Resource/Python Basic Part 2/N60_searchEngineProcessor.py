from src.__modules__ import challenge

class N60_searchEngineProcessor(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/basic/python-basic-1-exercise-60.php"
    self.challengeDescription = "Internet search engine giant, such as Google "\
      "accepts web pages around the world and classify them, creating a huge " \
      "database. The search engines also analyze the search keywords entered " \
      "by the user and create inquiries for database search. In both cases, " \
      "complicated processing is carried out in order to realize efficient " \
      "retrieval, but basics are all cutting out words from sentences. " \
      "Write a Python program to cut out words of 3 to 6 characters length " \
      "from a given sentence not more than 1024 characters. "
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    isValid = False
    searchQuery = ""

    while not isValid:
      searchQuery = input(
        "Enter a search query (maximum of 1024 characters)\n\t"
      )

      if not 0 < len(searchQuery) < 1025:
        print("Invalid search query length\n")
        continue

      isValid = True
    print("Search query processed to contain 3 - 6 characters:", end = "\n\t")
    print(" ".join([w for w in searchQuery.split(" ") if 2 < len(w) < 7]))

if __name__ == "__main__":
  main = N60_searchEngineProcessor()
  main.execute()