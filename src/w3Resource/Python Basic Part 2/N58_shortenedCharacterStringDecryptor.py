import re
from src.__modules__ import challenge

class N58_shortenedCharacterStringDecryptor(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/basic/python-basic-1-exercise-58.php"
    self.challengeDescription = "When character are consecutive in a string , "\
      "it is possible to shorten the character string by replacing the " \
      "character with a certain rule. For example, in the case of the " \
      "character string YYYYY, if it is expressed as # 5 Y, it is compressed " \
      "by one character. Write a Python program to restore the original " \
      "string by entering the compressed string with this rule. However, the " \
      "# character does not appear in the restored character string. \n" \
      " Sample Output:\n  Original text: XY#6Z1#4023\n  XYZZZZZZ1000023\n" \
      "  Original text: #39+1=1#30\n  999+1=1000"
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    isValid = False
    compressed = ""
    while not isValid:
      compressed = input("Enter compressed string (Maximum of 100 characters): ")
      if not 0 < len(compressed) < 101:
        print("Invalid string length\n")
        continue
      isValid = True
    
    decrypted = compressed
    for short in re.findall(r"#[\d].",compressed):
      decrypted = decrypted.replace(short, short[2] * int(short[1]), 1)
    print(f'Restored text: {decrypted}')
    



if __name__ == "__main__":
  main = N58_shortenedCharacterStringDecryptor()
  main.execute()