from src.__modules__ import challenge, objectListInput
from importlib import import_module
QuickSortClass = getattr(
  import_module("src.w3Resource.Python Data Structures - Sorting"), "N09_quickSort"
)

class N01_binarySearch(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-1.php"
    self.challengeDescription = "Write a Python program for binary search."
    self.timeComplexity = "O(log n) - Logarithmic Time"

  def execute(self) -> None:
    arrayToSearch = [9, 1, 8, 2, 7, 3, 6, 4, 5]
    query = 7

    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToSearch = []
        break
      print("Please enter a valid answer\n")

    if arrayToSearch == []:
      arrayToSearch = objectListInput(int)
      while True:
        try:
          query = int(input("Enter search value: "))
        except ValueError:
          print("Search query must be a valid integer.\n")
          continue
        break
    else:
      print("Array to search values are:", arrayToSearch)
      print("Search value is: ", query)

    quickSortClass = QuickSortClass()
    quickSortClass.quickSortAscending(
      arrayToSearch,
      0,
      len(arrayToSearch) - 1,
      display = False
    )
    
    print("Sorted array values are: ", arrayToSearch)
    print("Query results are:", end = " ")
    print(self.binarySearch(arrayToSearch,query))

  def binarySearch(self, arr: list, query: int) -> dict:
    firstKey = 0
    lastKey = len(arr) - 1

    while True:
      curKey = -(-(lastKey - firstKey) // 2) + firstKey
      curValue = arr[curKey]
      if query == curValue:
        return { "found": True, "index": curKey}
      elif firstKey == curKey:
        return { "found": False, "index": -1}
      elif query < curValue:
        lastKey = curKey - 1
      else:
        firstKey = curKey + 1

if __name__ == "__main__":
  main = N01_binarySearch()
  main.execute()