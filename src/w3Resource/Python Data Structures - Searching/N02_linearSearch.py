from src.__modules__ import challenge, objectListInput

class N02_linearSearch(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/data-structures-and-algorithms/python-search-and-sorting-exercise-2.php"
    self.challengeDescription = "Write a Python program for sequential search/"\
      "linear search."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    arrayToSearch = [11,23,58,31,56,77,43,12,65,19]
    query = 31


    while True:
      userInput = input("Use default values? (y/n): ")
      if userInput.lower() == 'y':
        break
      elif userInput.lower() == 'n':
        arrayToSearch = []
        break
      print("Please enter a valid answer\n")

    if arrayToSearch == []:
      arrayToSearch = objectListInput(int)
      while True:
        try:
          query = int(input("Enter search value: "))
        except ValueError:
          print("Search query must be a valid integer.\n")
          continue
        break
    else:
      print("Array to search values are:", arrayToSearch)
      print("Search value is: ", query)
    
    print("Query results are:", end = " ")
    print(self.linearSearch(arrayToSearch,query))

  def linearSearch(self, arr: list, query: int) -> dict:
    for index, value in enumerate(arr):
      if value == query:
        return { "found": True, "index": index }
        break
    return { "found": False, "index": -1 }
if __name__ == "__main__":
  main = N02_linearSearch()
  main.execute()