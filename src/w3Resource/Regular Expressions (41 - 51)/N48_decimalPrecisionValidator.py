from src.__modules__ import challenge
import re

class N48_decimalPrecisionValidator(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-48.php"
    self.challengeDescription = "Write a Python program to check a decimal "\
      "with a precision of 2."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    decimals =  [
      123.11,
      123,
      0.21,
      123.1214,
      3.124587,
      "e666.86",
      ".25"
    ]

    print("Validation for valid decimal floats that have a precision of 2")
    for decimal in decimals:
      print(f'{decimal}:', self.validateDecimalPrecision(decimal))

    print()
    print("Validation for valid decimal floats within the precision of 0 to 2")
    for decimal in decimals:
      print(f'{decimal}:', self.validateDecimalWithinPrecision(decimal))


  def validateDecimalPrecision(self, value) -> bool:
    regex = r"^[\d]+(\.\d{2})$"
    if re.search(regex, str(value)):
      return True
    return False
  
  def validateDecimalWithinPrecision(self, value) -> bool:
    regex = r"^[\d]+(\.\d{1,2})?$"
    if re.search(regex, str(value)):
      return True
    return False

if __name__ == "__main__":
  main = N48_decimalPrecisionValidator()
  main.execute()