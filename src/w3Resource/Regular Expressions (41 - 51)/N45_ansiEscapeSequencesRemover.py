from src.__modules__ import challenge
import re

class N45_ansiEscapeSequencesRemover(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-45.php"
    self.challengeDescription = "Write a Python program to remove the ANSI "\
      "escape sequences from a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "\t\u001b[0;35mgoogle.com\u001b[0m \u001b[0;36m216.58.218.206"\
      "\u001b[0m"

    print(string, "=>", self.removeANSI(string))

  def removeANSI(self, string: str) -> str:
    regex = r"\x1b[^m]*m"
    return re.sub(regex, "", string)
  
if __name__ == "__main__":
  main = N45_ansiEscapeSequencesRemover()
  main.execute()