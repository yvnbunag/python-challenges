from src.__modules__ import challenge
import re

class N49_givenStringLengthRemover(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-49.php"
    self.challengeDescription = "Write a Python program to remove words from "\
      "a string of length between 1 and a given number."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    while True:
      try:
        upTo = int(input("Enter maximum length: "))
        break
      except ValueError:
        print("Please enter a valid integer\n")

    string =  "The quick brown fox jumps over the lazy dog."

    print("Original value:", string)
    print(
      f"Words with 1 - {upTo} characters stripped off:",
      self.removeGivenStringLength(string, upTo)
    )
    
  def removeGivenStringLength(self, string: str, upTo: int) -> str:
    regex = r"\W*\b\w{1," + re.escape(str(upTo)) + r"}\b"
    replace = r""
    return re.sub(regex, replace, string)

if __name__ == "__main__":
  main = N49_givenStringLengthRemover()
  main.execute()