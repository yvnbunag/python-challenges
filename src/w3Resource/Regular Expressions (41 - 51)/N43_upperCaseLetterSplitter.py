from src.__modules__ import challenge
import re

class N43_upperCaseLetterSplitter(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-43.php"
    self.challengeDescription = "Write a Python program to split a string at "\
      "uppercase letters."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "PythonTutorialAndExercises"
    print(string, "=>", self.splitOnUpperCase(string))

  def splitOnUpperCase(self, string: str) -> list:
    regex = r"[A-Z][^A-Z]*"
    return re.findall(regex, string)
  
if __name__ == "__main__":
  main = N43_upperCaseLetterSplitter()
  main.execute()