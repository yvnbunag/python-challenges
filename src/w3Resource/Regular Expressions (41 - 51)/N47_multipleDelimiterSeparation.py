from src.__modules__ import challenge
import re

class N47_multipleDelimiterSeparation(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-47.php"
    self.challengeDescription = "Write a Python program to split a string "\
      "with multiple delimiters."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "The,quick;brown\nfox jumps*over the lazy dog."
    print(string, "=>", self.splitDelimiters(string))

  def splitDelimiters(self, string: str) -> str:
    regex = r",|;|\n|\*"
    return re.split(regex, string)
  
  def validateDecimalWithinPrecision(self, value) -> bool:
    regex = r"^[\d]+(\.\d{1,2})?$"
    if re.search(regex, str(value)):
      return True
    return False

if __name__ == "__main__":
  main = N47_multipleDelimiterSeparation()
  main.execute()