from src.__modules__ import challenge
import re

class N51_capitalWordSpacer(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-51.php"
    self.challengeDescription = "Write a Python program to insert spaces "\
      "between words starting with capital letters."
    self.timeComplexity = ""

  def execute(self) -> None:
    strings = [
      "Python",
      "PythonExercises",
      "PythonExercisesPracticeSolution"
    ]

    for string in strings:
      print("LIST METHOD::",string,"=>",self.toListMethod(string))
      print("REPL METHOD::",string,"=>",self.splitMethod(string))
    
  def splitMethod(self, string: str) -> str:
    regex = r"(\w)([A-Z])"
    replace = r"\1 \2"
    return re.sub(regex, replace, string)

  def toListMethod(self, string: str) -> str:
    regex = r"[A-Z][^A-Z]+"
    matches = re.findall(regex, string)
    return " ".join(matches)

if __name__ == "__main__":
  main = N51_capitalWordSpacer()
  main.execute()