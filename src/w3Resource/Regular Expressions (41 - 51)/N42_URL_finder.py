from src.__modules__ import challenge
import re

class N42_URL_finder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-42.php"
    self.challengeDescription = "Write a Python program to find urls in a "\
      "string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = '<p>Contents :</p><a href="https://w3resource.com">Python '\
      'Examples</a><a href="http://github.com">Even More Examples</a>'
    print(string)
    print("\nURL's on string: ")
    for URL in self.extractURL(string):
      print(URL)

  def extractURL(self, string: str) -> list:
    regex = r"https?://\w+.[a-zA-z]{2,3}"
    return re.findall(regex, string)
  
if __name__ == "__main__":
  main = N42_URL_finder()
  main.execute()