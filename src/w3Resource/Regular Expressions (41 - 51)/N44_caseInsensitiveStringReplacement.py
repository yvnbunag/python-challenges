from src.__modules__ import challenge
import re

class N44_caseInsensitiveStringReplacement(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-44.php"
    self.challengeDescription = "Write a Python program to do a "\
      "case-insensitive string replacement."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "PHP Exercises"
    stringToMatch = "php"
    stringToReplace = "Python"

    print(f'Original Text:  {string}')
    print(f'Using {stringToMatch} replace with {stringToReplace}')

    replacedString = self.replaceString(
      string,
      stringToMatch,
      stringToReplace
    )
    print(f'Replaced value: {replacedString}')

  def replaceString(
    self,
    string: str,
    stringToMatch: str,
    stringToReplace: str
  ) -> str:
    regex = r"(?i)" + stringToMatch
    return re.sub(regex, stringToReplace, string)
  
if __name__ == "__main__":
  main = N44_caseInsensitiveStringReplacement()
  main.execute()