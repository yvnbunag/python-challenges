from src.__modules__ import challenge
import re

class N50_parenthesisAreaRemover(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-50.php"
    self.challengeDescription = "Write a Python program to remove the "\
      "parenthesis area in a string."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    strings =  [
      "example (.com)",
      "w3resource",
      "github (.com)",
      "stackoverflow (.com)"
    ]

    for string in strings:
      print(string,"=>",self.removeParenthesisArea(string))
    
  def removeParenthesisArea(self, string: str) -> str:
    regex = r"\(.*\)"
    replace = r""
    return re.sub(regex, replace, string)

if __name__ == "__main__":
  main = N50_parenthesisAreaRemover()
  main.execute()