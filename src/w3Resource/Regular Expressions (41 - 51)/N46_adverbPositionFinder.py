from src.__modules__ import challenge
import re

class N46_adverbPositionFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-46.php"
    self.challengeDescription = "Write a Python program to find all adverbs "\
      "and their positions in a given sentence."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "Clearly, he has no excuse for such behavior he displayed "\
      "inappropriately."

    for match in self.findAdverbs(string):
      print(match)

  def findAdverbs(self, string: str) -> str:
    regex = r"\b\w+ly\b"
    iterResults = re.finditer(regex, string)
    return ['{0}-{1}: "{2}"'.format(
        m.start(),
        m.end(),
        m.group()
      ) for m in iterResults]
  
if __name__ == "__main__":
  main = N46_adverbPositionFinder()
  main.execute()