from src.__modules__ import challenge
import re

class N41_nonAlphanumericRemover(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-41.php"
    self.challengeDescription = "Write a Python program to find urls in a "\
      "string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "**//Python _Exercises_// - 12. "
    print(string, "=>", self.removeNoneAlpha(string))

  def removeNoneAlpha(self, string: str) -> str:
    regex = r"[\W_]+"
    return re.sub(regex, "", string)
  
if __name__ == "__main__":
  main = N41_nonAlphanumericRemover()
  main.execute()