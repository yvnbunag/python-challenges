from src.__modules__ import challenge
import re

class N31_allOccurrenceReplacer(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-31.php"
    self.challengeDescription = "Write a Python program to replace all "\
      "occurrences of space, comma, or dot with a colon."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "Python Exercises, PHP exercises."
    print(string, "=>", self.replaceAllOccurrence(string))

  def replaceAllOccurrence(self, string: str) -> str:
    regex = r'[\s,\.]'
    return re.sub(regex, ":", string)
  
if __name__ == "__main__":
  main = N31_allOccurrenceReplacer()
  main.execute()