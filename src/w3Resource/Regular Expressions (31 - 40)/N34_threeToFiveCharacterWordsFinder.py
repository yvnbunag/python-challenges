from src.__modules__ import challenge
import re

class N34_threeToFiveCharacterWordsFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-34.php"
    self.challengeDescription = "Write a Python program to find all three, "\
      "four, five characters long words in a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "The quick brown fox jumps over the lazy dog longer wordings."
    print(string, "=>", self.collectThreeToFiveCharacters(string))

  def collectThreeToFiveCharacters(self, string: str) -> list:
    regex = r'\b\w{3,5}\b'
    return re.findall(regex, string)
  
if __name__ == "__main__":
  main = N34_threeToFiveCharacterWordsFinder()
  main.execute()