from src.__modules__ import challenge
import re

class N33_fiveCharacterWordsFinder(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-33.php"
    self.challengeDescription = "Write a Python program to find all five "\
      "characters long word in a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "The quick brown fox jumps over the lazy dog."
    print(string, "=>", self.findFiveCharacterWords(string))

  def findFiveCharacterWords(self, string: str) -> list:
    regex = r'\b\w{5}\b'
    return re.findall(regex, string)
  
if __name__ == "__main__":
  main = N33_fiveCharacterWordsFinder()
  main.execute()