from src.__modules__ import challenge
import re

class N35_minimumStringLengthCollector(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-35.php"
    self.challengeDescription = "Write a Python program to find all words "\
      "which are at least 4 characters long in a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "The quick brown fox jumps over the lazy dog."
    print(string, "=>", self.collectMinimumStringLength(string))

  def collectMinimumStringLength(self, string: str) -> list:
    regex = r'\b\w{4,}\b'
    return re.findall(regex, string)
  
if __name__ == "__main__":
  main = N35_minimumStringLengthCollector()
  main.execute()