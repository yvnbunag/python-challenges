from src.__modules__ import challenge
import re

class N38_quotationMarkValue(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-38.php"
    self.challengeDescription = "Write a Python program to extract values "\
      "between quotation marks of a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = '"Python", "PHP", "Java"'
    print(string, "=>", self.extractQuotationValues(string))

  def extractQuotationValues(self, string: str) -> list:
    regex = r'"(.+?)"'
    return re.findall(regex, string)
  
if __name__ == "__main__":
  main = N38_quotationMarkValue()
  main.execute()