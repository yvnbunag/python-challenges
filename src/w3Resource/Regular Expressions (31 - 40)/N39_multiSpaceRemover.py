from src.__modules__ import challenge
import re

class N39_multiSpaceRemover(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-39.php"
    self.challengeDescription = "Write a Python program to remove multiple "\
      "spaces in a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = " Python    Exercises 2 "
    print(f'"{string}"', "=>", self.removeMultipleWhiteSpaces(string))

  def removeMultipleWhiteSpaces(self, string: str) -> str:
    regex = r"[\s]{2,}"
    return re.sub(regex, " ", f'"{string}"')
  
if __name__ == "__main__":
  main = N39_multiSpaceRemover()
  main.execute()