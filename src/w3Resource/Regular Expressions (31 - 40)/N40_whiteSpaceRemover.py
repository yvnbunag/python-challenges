from src.__modules__ import challenge
import re

class N40_whiteSpaceRemover(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-40.php"
    self.challengeDescription = "Write a Python program to remove all "\
      "whitespaces from a string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = " Python    Exercises "
    print(string, "=>", self.removeWhiteSpaces(string))

  def removeWhiteSpaces(self, string: str) -> str:
    regex = r"[\s]+"
    return re.sub(regex, "", string)
  
if __name__ == "__main__":
  main = N40_whiteSpaceRemover()
  main.execute()