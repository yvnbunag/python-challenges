from src.__modules__ import challenge
import re

class N37_snakeToCamelCase(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-37.php"
    self.challengeDescription = "Write a python program to convert snake case "\
      "string to camel case string."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "python_exercises_snake_to_1camel"
    print(string, "=>", self.snakeToCamel(string))

  def snakeToCamel(self, string: str) -> str:
    regex = r'(_)([\w])'
    replacement = lambda x: x.group(2).upper()
    return re.sub(regex, replacement, string)
  
if __name__ == "__main__":
  main = N37_snakeToCamelCase()
  main.execute()