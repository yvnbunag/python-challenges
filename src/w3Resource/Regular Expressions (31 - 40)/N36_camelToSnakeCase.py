from src.__modules__ import challenge
import re

class N36_camelToSnakeCase(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-36.php"
    self.challengeDescription = "Write a python program to convert camel case "\
      "string to snake case string"
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "pythonExercisesCamelTo1snake2Snake"
    print(string, "=>", self.camelToSnake(string))

  def camelToSnake(self, string: str) -> str:
    regex = r'[A-Z0-9]'
    replacement = lambda x: f'_{x.group().lower()}'
    return re.sub(regex, replacement, string)
  
if __name__ == "__main__":
  main = N36_camelToSnakeCase()
  main.execute()