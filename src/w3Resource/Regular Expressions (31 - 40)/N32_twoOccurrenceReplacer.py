from src.__modules__ import challenge
import re

class N32_twoOccurrenceReplacer(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.w3resource.com/python-exercises/re/python-re-exercise-32.php"
    self.challengeDescription = "Write a Python program to replace maximum 2 "\
      "occurrences of space, comma, or dot with a colon."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    string = "Python Exercises, PHP exercises."
    print(string, "=>", self.replaceTwoOccurrence(string))

  def replaceTwoOccurrence(self, string: str) -> str:
    regex = r'[\s,\.]'
    return re.sub(regex, ":", string, 2)
  
if __name__ == "__main__":
  main = N32_twoOccurrenceReplacer()
  main.execute()