import re
from colorama import Fore

def formatDir(dirName: str) -> str:
  """ 
    Formats directory name by splitting and adding whitespaces from the
    camelcase convention and replacing underscores with whitespaces
  """
  dirNameSplit = re.sub('(?!^)([A-Z][a-z]+)', r' \1', dirName).split()
  separator = " "
  dirNameFormatted = separator.join(dirNameSplit)
  return dirNameFormatted.replace("_", " ").title()

def formatChal(challengeName: str) -> str:
  """
    Formats directory name by splitting and adding whitespaces from the
    camelcase convention, replacing underscores with whitespaces and stripping
    out the file extension
  """
  challengeNameSpaced = challengeName.replace("_", " ").replace(".py","")
  chalNameSplit = re.sub('(?!^)([A-Z][a-z]+)', r' \1', challengeNameSpaced).split()
  for index, word in enumerate(chalNameSplit):
    chalNameSplit[index] = word[0].upper() + word[1:]
  
  separator = " "
  chalNameFormatted = separator.join(chalNameSplit)
  return chalNameFormatted

def colored(string: str, stype: str = None) -> str:
  """
    Prepends ANSI escape character sequences at the start of the string to 
    change its text display color
  """
  collection = {
    "GREEN": ["directory", "finished", "quickSortPivot"],
    "YELLOW": ["challenge"],
    "BLUE": ["back"],
    "CYAN": ["dirList", "quickSortPartition"],
    "RED": ["exit", "error"],
    "MAGENTA": ["start"]
  }

  if stype in collection['GREEN']:
    return f'{Fore.GREEN}{string}'
  elif stype in collection['YELLOW']:
    return f'{Fore.YELLOW}{string}'
  elif stype in collection['BLUE']:
    return f'{Fore.BLUE}{string}'
  elif stype in collection['CYAN']:
    return f'{Fore.CYAN}{string}'
  elif stype in collection['RED']:
    return f'{Fore.RED}{string}'
  elif stype in collection['MAGENTA']:
    return f'{Fore.MAGENTA}{string}'
  else:
    return string
