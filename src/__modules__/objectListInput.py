def objectListInput(
  varType,
  minimum:int = 2,
  maximum:int = 999,
  promptType = "default"
  ) -> list:
  """
    Prompts the user to enter a sequence of inputs to be stored in a list
  """
  isValid = False
  inputType = ""
  iRange = ""
  prompt = ""
  listObject = []
  if varType == int:
    inputType = "integer"
  elif varType == float:
    inputType = "float"
  else:
    inputType = "string"
  
  if minimum == maximum:
    iRange = minimum
  else:
    iRange = f'{minimum} - {maximum}'
  

 
  while not isValid:
    if promptType == "default":
      prompt = f'Enter valid {inputType} list ({iRange} variables delimited by space):\n\t'
    elif promptType == "tabbed":
      prompt = "\t"

    listObject = [var for var in input(prompt).split()]

    if len(listObject) < minimum:
      print(
        "Input error: Minimum list size is " \
        f'{minimum}, current size: {len(listObject)}.' \
        "Please reenter values:"
      )
      continue

    if len(listObject) > maximum:
      print(
        "Input error - Maximum list size is " \
        f'{maximum}, current size: {len(listObject)}. ' \
        "Please reenter values:"
      )
      continue     

    try:
      if varType == int:
        for index, var in enumerate(listObject):
          listObject[index] = int(var)
      elif varType == float:
        for index, var in enumerate(listObject):
          listObject[index] = float(var)
    except ValueError:
      print("Input error - Invalid input type. Please reenter values:")
      continue
    
    isValid = True

  return listObject
      
    



  


  
    