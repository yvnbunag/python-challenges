from typing import Type

class Node():
  def __init__(self, value = None):
    self.value = value
    self.leftChild = None
    self.rightChild = None

  def insert(self, nextValue) -> None:
    if self.value is not None:
      if nextValue <= self.value:
        if self.leftChild is None:
          self.leftChild = Node(nextValue)
        else:
          self.leftChild.insert(nextValue)
      elif nextValue > self.value:
        if self.rightChild is None:
          self.rightChild = Node(nextValue)
        else:
          self.rightChild.insert(nextValue)
    else:
      self.value = nextValue
  
  def find(self, searchQuery: int) -> int:
    index = 0
    rootNode = self
    nodeStack = []
    while rootNode or nodeStack:
      while rootNode:
        nodeStack.append(rootNode)
        rootNode = rootNode.leftChild
      
      rootNode = nodeStack.pop()

      if rootNode.value == searchQuery:
        break      
      index += 1
      rootNode = rootNode.rightChild 

    if not rootNode:
      return -1
    return index

  def __getitem__(self, index) -> int:
    index = int(index)

    rootNode = self
    nodeStack = []
    while rootNode or nodeStack:
      while rootNode:
        nodeStack.append(rootNode)
        rootNode = rootNode.leftChild
      
      rootNode = nodeStack.pop()
      
      if index == 0:
        break
      index -= 1

      rootNode = rootNode.rightChild
    
    if rootNode:
      return rootNode.value
    else:
      raise IndexError

  def deleteNode(
    self,
    deleteQuery,
    beforeNode = None,
    direction = None
  ) -> bool:
    if self.value == deleteQuery:
      if self.leftChild and self.rightChild:
        replacementChild = self.rightChild
        while replacementChild.leftChild:
          replacementChild = replacementChild.leftChild
        temporaryValue = replacementChild.value
        self.deleteNode(temporaryValue)
        self.value = temporaryValue
          
      elif self.leftChild and not self.rightChild:
        self.__dict__.update(self.leftChild.__dict__)

      elif self.rightChild and not self.leftChild:
        self.__dict__.update(self.rightChild.__dict__)

      else:
        if beforeNode:
          if direction == "left":
            beforeNode.leftChild = None
          elif direction == "right":
            beforeNode.rightChild = None

      return True
    elif deleteQuery < self.value and self.leftChild:
      return self.leftChild.deleteNode(deleteQuery, self, "left") 
    elif deleteQuery > self.value and self.rightChild:
      return self.rightChild.deleteNode(deleteQuery, self, "right")
    else:
      return False
  
  def balanceSelf(self) -> None:
    sortedArray = Node.getAscendingReturned(self)
    self.__dict__.update(
      Node.createBalancedNode(sortedArray).__dict__
    )

  @staticmethod
  def createBalancedNode(sortedArray: list):
    if not sortedArray:
        return None
    center = len(sortedArray) // 2
    node = Node(sortedArray[center])
    node.leftChild = Node.createBalancedNode(sortedArray[:center])
    node.rightChild = Node.createBalancedNode(sortedArray[center+1:])
    return node

  @staticmethod
  def nodeOrderView(node): 
    if not node: 
        return      
    print(f'[{node.value}]')
    print("L-> ", end = "")
    Node.nodeOrderView(node.leftChild)
    print("R-> ", end = "")
    Node.nodeOrderView(node.rightChild)

  
  @staticmethod
  def getAscending(rootNode, collection) -> None:
    if rootNode:
      Node.getAscending(rootNode.leftChild, collection)
      collection.append(rootNode.value)
      Node.getAscending(rootNode.rightChild, collection)

  @staticmethod
  def getAscendingReturned(rootNode) -> list:
    collection = []
    if rootNode:
      collection += Node.getAscendingReturned(rootNode.leftChild)
      collection += [rootNode.value]
      collection += Node.getAscendingReturned(rootNode.rightChild)
    return collection

  @staticmethod
  def getClosestTargetValue(rootNode, targetValue) -> int:
    rootValue = rootNode.value
    childNode = rootNode.leftChild if targetValue < rootValue else rootNode.rightChild
    if not childNode:
        return rootValue
    childValue = Node.getClosestTargetValue(childNode, targetValue)
    return min((rootValue,childValue), key=lambda x: abs(targetValue-x))

  @staticmethod
  def validateBinaryTree(node) -> bool:
    if not node:
      return True

    leftValue = node.leftChild.value if node.leftChild else node.value - 1
    rightValue = node.rightChild.value if node.rightChild else node.value + 1

    nodeValidity = leftValue < node.value < rightValue

    return (
      Node.validateBinaryTree(node.leftChild) and 
      nodeValidity and
      Node.validateBinaryTree(node.rightChild)
    )