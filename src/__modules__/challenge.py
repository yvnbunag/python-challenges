from src.__modules__ import customString as cs

class challenge():
  def __init__(self):
    """
      Instantiates the challenge title, placeholder URL and description
    """
    self.challengeTitle = cs.formatChal(self.__class__.__name__)
    self.challengeURL = "No URL link provided"
    self.challengeDescription = "No "\
      "description " \
      "provided"
    self.timeComplexity = "No Time Complexity provided"

  def execute(self) -> None:
    """
      Method to be run by the main class which is required for all challenges
    """
    errorMessage = f'EXECUTE:: {self.challengeTitle} has no executable script'
    print(cs.colored(errorMessage, "error"))

  def getTitle(self) -> str:
    """
      Returns the title of the challenge
    """
    return self.challengeTitle

  def getURL(self) -> str:
    """
      Returns the URL link of the source of the challenge
    """
    return self.challengeURL

  def getDescription(self) -> str:
    """
      Returns the description/requirements of the challenge
    """
    return self.challengeDescription

  def getTimeComplexity(self) -> str:
    """
      Returns the time complexity of the challenge
    """
    return self.timeComplexity