def levelPrint(level: int, string: str) -> None:
  """ Adds extra space at the start of the string before printing """
  spacing = ""
  while(level>0):
    spacing += " "
    level -= 1
  print(f'{spacing}{string}')