from src.__modules__ import challenge
from .__modules__.SingletonPattern import EventLogger

class SingletonPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/singleton_pattern.htm"
    self.challengeDescription = "This pattern involves a single class which "\
      "is responsible to create an object while making sure that only single "\
      "object gets created. This class provides a way to access its only "\
      "object which can be accessed directly without need to instantiate the "\
      "object of the class."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    EventLoggers = list(range(5))
    for ctr, object in enumerate(EventLoggers):
      print(f'Iteration {ctr+1}: ')
      try:
        if ctr % 2 == 1:
          # Get EventLogger instance thru getInstance() method
          EventLoggers[ctr] = EventLogger.getInstance()
        else:
          # Create EventLogger instance thru construction
          EventLoggers[ctr] = EventLogger()

      except Exception as err:
        print(err, end = "\n\n")
        continue

      EventLoggers[ctr].log(
        f'Counter {ctr+1} reference of EventLogger Singleton Class'
      )
      print("Instance details:")
      print(EventLoggers[ctr], end = "\n\n")

if __name__ == "__main__":
  main = SingletonPattern()
  main.execute()