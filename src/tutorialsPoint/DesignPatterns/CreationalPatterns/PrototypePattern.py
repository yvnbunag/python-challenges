from src.__modules__ import challenge
from .__modules__.PrototypePattern import SingletonShapeStore
from .__modules__.PrototypePattern import DatabaseRequester

class PrototypePattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/prototype_pattern.htm"
    self.challengeDescription = "This pattern involves implementing a "\
      "prototype interface which tells to create a clone of the current "\
      "object. This pattern is used when creation of object directly is "\
      "costly. For example, an object is to be created after a costly "\
      "database operation. We can cache the object, returns its clone on next "\
      "request and update the database as and when needed thus reducing "\
      "database calls."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    print("<<Class Store Example>>")
    print("Initializing ShapeStore")
    # Class Prototype Factory
    shapeStore = SingletonShapeStore.getInstance()
    shapeStore2 = SingletonShapeStore.getInstance()

    # Creating Heavy Circle object from ShapeStore Prototype Factory
    heavyCircle = shapeStore.getShape("HeavyCircle")

    print("Heavy circle prototype memory address: ", end = "")
    print(shapeStore.getPrototypeAddress("HeavyCircle"))
    print(
      "Heavy circle prototype memory address from second reference " \
      "instance:", end = " "
    )
    print(shapeStore2.getPrototypeAddress("HeavyCircle"))
    print()

    print(
      f'{heavyCircle.getShape()} object instance deep cloned from prototype'
    )
    print(f'First heavy circle object memory address: {hex(id(heavyCircle))}')

    print()

    heavyCircle2 = shapeStore.getShape("HeavyCircle")
    print(
      f'{heavyCircle2.getShape()} object instance deep cloned from prototype'
    )
    print(f'Second heavy circle object memory address: {hex(id(heavyCircle2))}')

    print()
    print("<<Object Cache Example>>")
    print("Initializing Database")

    # Mock Databases
    db = DatabaseRequester()

    # Mock Fetching
    print("\nFetching database object with ID == 1")
    object1 = db.get("1")
    print(f'[1]{object1.getShape()} object instance from database get')

    print("\nRefetching database object with ID == 1")
    object1 = db.get("1")

    print(
      f'\nReassigning [1]{object1.getShape()} object from cache to another type'
    )
    object1 = shapeStore.getShape("HeavySquare")
    print(
      f'{object1.getShape()} object clone from ShapeStore prototype and ' \
      'assigned to cache object container'
    )

    print("\nRefetching database object with ID == 1 from cache")
    object1 = db.get("1")
    print(f'[1]{object1.getShape()} object prototype from cache unchanged')

    print(
      f'\nUpdating [1]{object1.getShape()} object in the database, with the ' \
      'update operation returning updated object from the database'
    )

    object1 = db.update("1", shapeStore.getShape("HeavyRectangle"))
    print(f'[1]{object1.getShape()} object prototype from cache updated')

    print("\nUpdating database object with ID == 2 (not stored in cache)")
    object2 = db.update("2", shapeStore.getShape("HeavySquare"))
    print(f'[2]{object2.getShape()} updated in the database')

    print("\nFetching database object with ID == 2")
    object2 = db.get("2")
    print(f'[2]{object2.getShape()} object instance from database get')



if __name__ == "__main__":
  main = PrototypePattern()
  main.execute()