from src.__modules__ import challenge
from .__modules__.AbstractFactoryPattern import FactoryProducer
from .__modules__.AbstractFactoryPattern.abstract import AbstractShapeFactory

class AbstractFactoryPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/abstract_factory_pattern.htm"
    self.challengeDescription = "In Abstract Factory pattern an interface is "\
      "responsible for creating a factory of related objects without " \
      "explicitly specifying their classes. Each generated factory can give " \
      "the objects as per the Factory pattern."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    shapeFactory: AbstractShapeFactory = FactoryProducer.getFactory()
    roundedShapeFactory: AbstractShapeFactory = FactoryProducer.getFactory("rounded")

    circle = shapeFactory.getShape("circle")
    circle.draw()
    rectangle = shapeFactory.getShape("rectangle")
    rectangle.draw()
    square = shapeFactory.getShape("square")
    square.draw()

    roundedRectangle = roundedShapeFactory.getShape("rectangle")
    roundedRectangle.draw()
    roundedSquare = roundedShapeFactory.getShape("square")
    roundedSquare.draw()
    
    

if __name__ == "__main__":
  main = AbstractFactoryPattern()
  main.execute()