from src.__modules__ import challenge
from .__modules__.ObjectPoolPattern import DBConnectionPool
class ObjectPoolPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.geeksforgeeks.org/object-pool-design-pattern/"
    self.challengeDescription = "pattern which is used in situations where "\
      "the cost of initializing a class instance is very high."\
      "Basically, an Object pool is a container which contains some amount of "\
      "objects. So, when an object is taken from the pool, it is not "\
      "available in the pool until it is put back."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    DBConnectionPool.setMaxPool(2)
    dbConnectionPool = DBConnectionPool.getInstance()


    dbConnectionPool.getPoolDetails()
    print()

    connectionA = dbConnectionPool.getConnection()
    dbConnectionPool.getPoolDetails()
    aConnected = connectionA.connect(
      host = "127.0.0.1",
      database = "mockDatabase",
      username = "mockUsernameA",
      password = "pwdA"
    )

    if self.validateConnection(aConnected):
      # Do things here
      pass
    else:
      connectionA.close()
      connectionA.returnToPool()

    print()
    connectionB = dbConnectionPool.getConnection()
    dbConnectionPool.getPoolDetails()
    bConnected = connectionB.connect(
      host = "127.0.0.1",
      database = "mockDatabase",
      username = "mockUsernameB",
      password = "pwdBB"
    )
    if self.validateConnection(bConnected):
      # Do things here
      pass
    else:
      connectionB.close()
      connectionB.returnToPool()

    print()
    connectionC = dbConnectionPool.getConnection()
    if not connectionC:
      print("Objects in pool currently in use")

    print()
    connectionB.close()
    connectionB.returnToPool()
    del connectionB
    dbConnectionPool.getPoolDetails()

    print()
    connectionD = dbConnectionPool.getConnection()
    dbConnectionPool.getPoolDetails()
    dConnected = connectionD.connect(
      host = "127.0.0.2",
      database = "mockDatabase",
      username = "mockUsernameD",
      password = "pwdDDDD"
    )

    if self.validateConnection(dConnected):
      # Do things here
      pass
    else:
      connectionD.returnToPool()

    print()
    connectionE = dbConnectionPool.getConnection()
    dbConnectionPool.getPoolDetails()
    eConnected = connectionE.connect(
      host = "127.0.0.1",
      database = "mockDatabase",
      username = "mockUsernameE",
      password = "pwdEEEEE"
    )

    print()
    connectionA.close()
    connectionA.returnToPool()
    del connectionA
    dbConnectionPool.getPoolDetails()

    print()
    connectionE.returnToPool()
    del connectionE
    dbConnectionPool.getPoolDetails()

    if self.validateConnection(eConnected):
      # Do things here
      pass
    else:
      connectionE.close()
      connectionE.returnToPool()
  
  def validateConnection(self, connection) -> bool:
    if connection["success"]:
      return True
    print("Connection failed. Reason:", connection["reason"])
    return False

if __name__ == "__main__":
  main = ObjectPoolPattern()
  main.execute()