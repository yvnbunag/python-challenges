import os.path
from src.__modules__ import challenge
from .__modules__.FactoryPattern import ShapeFactory
from .__modules__.FactoryPattern.interfaces import Shape

class FactoryPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/factory_pattern.htm"
    self.challengeDescription = "In Factory pattern, we create object without "\
      "exposing the creation logic to the client and refer to newly created " \
      "object using a common interface."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    shapeFactory = ShapeFactory()

    circle: Shape = shapeFactory.getShape("Circle")
    rectangle: Shape = shapeFactory.getShape("Rectangle")
    square: Shape = shapeFactory.getShape("Square")

    circle.draw()
    rectangle.draw()
    square.draw()

if __name__ == "__main__":
  main = factoryPattern()
  main.execute()