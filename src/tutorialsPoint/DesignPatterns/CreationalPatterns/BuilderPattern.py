from src.__modules__ import challenge
from .__modules__.BuilderPattern import SimpleHTMLBuilder
from .__modules__.BuilderPattern import MarkupGenerator
from .__modules__.BuilderPattern import HTMLElementsFactory

class BuilderPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/builder_pattern.htm"
    self.challengeDescription = "A Builder class builds the final object step "\
      "by step. This builder is independent of other objects."
    self.timeComplexity = "O(n!) - Factorial Time"

  def execute(self) -> None:
    HTMLbuilder = SimpleHTMLBuilder()

    print(
      "Try copying the generated HTML code, paste the contents to a new HTML " \
      "file and open it in your browser. \n"
    )
    print("HTMLbuilder.buildSimpleHTML() generated object run thru MarkupGenerator: ")
    simpleHTML = HTMLbuilder.buildSimpleHTML()
    print(MarkupGenerator.transform(simpleHTML))

    print("\nHTMLbuilder.buildFormHTML() generated object run thru MarkupGenerator: ")
    formHTML = HTMLbuilder.buildFormHTML()
    print(MarkupGenerator.transform(formHTML))


if __name__ == "__main__":
  main = BuilderPattern()
  main.execute()