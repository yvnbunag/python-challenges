from .classes import Body, Br, Div, Form, Head, Html, Input, Span, Title

class HTMLElementsFactory():
  def create(self, element):
    if element == "body":
      return Body()
    if element == "br":
      return Br()
    if element.lower() == "div":
      return Div()
    if element.lower() == "form":
      return Form()
    if element.lower() == "head":
      return Head()
    if element.lower() == "html":
      return Html()
    if element.lower() == "input":
      return Input()
    if element.lower() == "span":
      return Span()
    if element.lower() == "title":
      return Title()
    return None