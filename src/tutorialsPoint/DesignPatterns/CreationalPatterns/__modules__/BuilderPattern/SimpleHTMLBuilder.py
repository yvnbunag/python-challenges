from . import HTMLElementsFactory
from .classes import Body, Html
class SimpleHTMLBuilder:
  def buildSimpleHTML(self):
    elementsFactory = HTMLElementsFactory()

    # head creation
    title = elementsFactory.create("title").setValue("Simple HTML Document")
    head = elementsFactory.create("head").addChild(title)

    # body creation
    messageSpan = elementsFactory.create("span") \
      .setValue("Hello World!") \
      .setAttribute("id", "messageSpan") \
      .setAttribute("style", "font-size: 40pt;")
    subMessageSpan = elementsFactory.create("span") \
      .setValue(
        "This is an HTML Document generated using a Python Builder Pattern " \
        "Class."
      ) \
      .setAttribute("class", "subMessageSpan") \
      .setAttribute("style", "font-size: 20pt; color: rgb(125, 125, 125)")

    innerDiv = elementsFactory.create("div") \
      .setAttribute(
        "style", "text-align: center"
      ) \
      .addChild(messageSpan) \
      .addChild(elementsFactory.create("br")) \
      .addChild(subMessageSpan)
      
    outerDiv = elementsFactory.create("div") \
      .setAttribute(
        "style", "position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%)"
      ) \
      .addChild(innerDiv)

    body = elementsFactory.create("body").addChild(outerDiv)

    root = elementsFactory.create("html") \
      .addChild(head) \
      .addChild(body)

    return root

  def buildFormHTML(self):
    elementsFactory = HTMLElementsFactory()

    # Head creation
    title = elementsFactory.create("title") \
      .setValue("Simple HTML Form")
    head = elementsFactory.create("head") \
      .addChild(title)

    # Body creation
    titleSpan = elementsFactory.create("span") \
      .setValue("HTML simple form") \
      .setAttribute("class", "formTitle") \
      .setAttribute("style", "font-size: 20pt")

    subTitleSpan = elementsFactory.create("span") \
      .setValue("Generated using Python Builder Pattern Class") \
      .setAttribute("class", "formSubTitle") \
      .setAttribute("style", "font-size: 16pt; color: rgba(125, 125, 125)")  

    firstNameLabel = elementsFactory.create("span") \
      .setValue("First name") \
      .setAttribute("style",  "font-weight: bold; font-size: 14pt")

    firstNameInput = elementsFactory.create("input") \
      .setAttribute("type", "text") \
      .setAttribute("id", "firstName") \
      .setAttribute("name", "firstName") \
      .setAttribute("placeholder", "First Name")

    middleNameLabel = elementsFactory.create("span") \
      .setValue("Middle name") \
      .setAttribute("style",  "font-weight: bold; font-size: 14pt")

    middleNameInput = elementsFactory.create("input") \
      .setAttribute("type", "text") \
      .setAttribute("id", "middleName") \
      .setAttribute("name", "middleName") \
      .setAttribute("placeholder", "Middle Name")

    lastNameLabel = elementsFactory.create("span") \
      .setValue("Last name") \
      .setAttribute("style",  "font-weight: bold; font-size: 14pt")

    lastNameInput = elementsFactory.create("input") \
      .setAttribute("type", "text") \
      .setAttribute("id", "lastName") \
      .setAttribute("name", "lastName") \
      .setAttribute("placeholder", "Last Name")

    ageLabel = elementsFactory.create("span") \
      .setValue("Age") \
      .setAttribute("style",  "font-weight: bold; font-size: 14pt")

    ageInput = elementsFactory.create("input") \
      .setAttribute("type", "number") \
      .setAttribute("id", "age") \
      .setAttribute("name", "age") \
      .setAttribute("value", 0) \
      .setAttribute("min", 0) \

    submitButton = elementsFactory.create("input") \
      .setAttribute("type", "submit") \
      .setAttribute("id", "submit") \
      .setAttribute("name", "submit") \
      .setAttribute("value", "Submit")

    form = elementsFactory.create("form") \
      .setAttribute("id", "generatedForm") \
      .setAttribute("method", "GET") \
      .setAttribute("target", "_blank") \
      .addChild(elementsFactory.create("br")) \
      .addChild(firstNameLabel) \
      .addChild(elementsFactory.create("br")) \
      .addChild(firstNameInput) \
      .addChild(elementsFactory.create("br")) \
      .addChild(elementsFactory.create("br")) \
      .addChild(middleNameLabel) \
      .addChild(elementsFactory.create("br")) \
      .addChild(middleNameInput) \
      .addChild(elementsFactory.create("br")) \
      .addChild(elementsFactory.create("br")) \
      .addChild(lastNameLabel) \
      .addChild(elementsFactory.create("br")) \
      .addChild(lastNameInput) \
      .addChild(elementsFactory.create("br")) \
      .addChild(elementsFactory.create("br")) \
      .addChild(ageLabel) \
      .addChild(elementsFactory.create("br")) \
      .addChild(ageInput) \
      .addChild(elementsFactory.create("br")) \
      .addChild(elementsFactory.create("br")) \
      .addChild(submitButton)

    titleDiv = elementsFactory.create("div") \
      .setAttribute(
        "style", "text-align: center"
      ) \
      .addChild(titleSpan) \
      .addChild(elementsFactory.create("br")) \
      .addChild(subTitleSpan)

    formDiv = elementsFactory.create("div") \
      .setAttribute(
        "style", "text-align: center"
      ) \
      .addChild(form)

    outerDiv = elementsFactory.create("div") \
      .setAttribute(
        "style", "position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%)"
      ) \
      .addChild(titleDiv) \
      .addChild(elementsFactory.create("br")) \
      .addChild(formDiv)

    body = elementsFactory.create("body") \
      .addChild(outerDiv)

    root = elementsFactory.create("html") \
      .addChild(head) \
      .addChild(body)
    return root