from ..abstract import HTMLElement

class Title(HTMLElement):
  def getName(self):
    return "title"