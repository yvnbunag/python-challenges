from ..abstract import HTMLElement

class Span(HTMLElement):
  def getName(self):
    return "span"