from ..abstract import HTMLElement

class Br(HTMLElement):
  def getName(self):
    return "br"

  def endTag(self):
    return False