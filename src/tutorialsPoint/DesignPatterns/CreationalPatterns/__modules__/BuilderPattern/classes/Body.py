from ..abstract import HTMLElement

class Body(HTMLElement):
  def getName(self):
    return "body"