from ..abstract import HTMLElement

class Head(HTMLElement):
  def getName(self):
    return "head"