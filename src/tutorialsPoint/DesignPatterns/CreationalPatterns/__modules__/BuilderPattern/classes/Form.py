from ..abstract import HTMLElement

class Form(HTMLElement):
  def getName(self):
    return "form"