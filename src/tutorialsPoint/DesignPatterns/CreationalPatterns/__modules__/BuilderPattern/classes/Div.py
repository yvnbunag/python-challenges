from ..abstract import HTMLElement

class Div(HTMLElement):
  def getName(self):
    return "div"