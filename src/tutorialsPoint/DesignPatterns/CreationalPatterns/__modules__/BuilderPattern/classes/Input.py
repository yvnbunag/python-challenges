from ..abstract import HTMLElement

class Input(HTMLElement):
  def getName(self):
    return "input"

  def endTag(self):
    return False