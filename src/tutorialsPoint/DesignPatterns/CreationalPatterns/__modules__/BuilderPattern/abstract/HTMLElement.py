from abc import ABC, abstractmethod
from interface import implements

class HTMLElement(ABC):
  def __init__(self):
    self.__children = []
    self.__attributes = {}
    self.__value = ""

  @abstractmethod
  def getName(self):
    pass

  def addChild(self, child):
    self.__children.append(child)
    return self

  def getChildren(self):
    return self.__children
  
  def setValue(self, value):
    self.__value = value
    return self

  def getValue(self):
    return self.__value

  def setAttribute(self, attrName, value):
    self.__attributes[attrName] = value
    return self

  def getAttribute(self, attrName):
    return self.__attributes[attrName]

  def getAttributes(self):
    return self.__attributes

  def endTag(self):
    return True