class MarkupGenerator():
  @staticmethod
  def transform(element):
    markup = ""
    name = element.getName()
    attributes = ""
    hasEndTag = element.endTag()

    # Format attributes
    for attrname, value in element.getAttributes().items():
      attributes += f' {attrname} = "{value}"'
    
    # Format starting tag
    startTag = f'<{name}{attributes} />' if not hasEndTag else f'<{name}{attributes}>'
    markup += f'{startTag}{element.getValue().replace(" ","&nbsp;")}'

    # Format element children
    for child in element.getChildren():
      markup += MarkupGenerator.transform(child)
    
    # Format ending tag
    if hasEndTag:
      markup += f'</{element.getName()}>'
    
    return markup