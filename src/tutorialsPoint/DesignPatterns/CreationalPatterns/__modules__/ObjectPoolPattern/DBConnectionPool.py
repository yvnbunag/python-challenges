from .classes import ReusableConnection

class DBConnectionPool():
  __instance = None
  __instantiationBlock = True
  __maxPool = 0
  __lockedPool = []
  __unlockedPool = []

  def __init__(self):
    if DBConnectionPool.__instantiationBlock:
      raise Exception(
        f'{self.__class__.__name__} is a singleton class and can only '\
        'be instantiated thru getInstance() static method'
      )
    else:
      DBConnectionPool.__instance = self

  def getConnection(self) -> bool:
    # If max pool is not yet reached, return a reusable object
    if len(DBConnectionPool.__lockedPool) < DBConnectionPool.__maxPool:
      if len(DBConnectionPool.__unlockedPool) == 0:
        # Create connection if none
        DBConnectionPool.__unlockedPool.append(ReusableConnection(self))
      reusableConnection = DBConnectionPool.__unlockedPool[0]
      reusableConnection.lock()
      return reusableConnection
    return False
  
  def lock(self, reusableConnection) -> None:
    DBConnectionPool.__unlockedPool.remove(reusableConnection)
    DBConnectionPool.__lockedPool.append(reusableConnection)
    print("Reusable connection taken from pool")
  
  def unlock(self, reusableConnection) -> None:
    DBConnectionPool.__lockedPool.remove(reusableConnection)
    DBConnectionPool.__unlockedPool.append(reusableConnection)
    print("Reusable connection inserted to pool")

  def getPoolDetails(self) -> None:
    print(
      "Current Pool: "\
      f'locked= {len(DBConnectionPool.__lockedPool)}, '\
      f'unlocked= {len(DBConnectionPool.__unlockedPool)}, '\
      f'max= {DBConnectionPool.__maxPool}'\
    )

  @staticmethod
  def setMaxPool(maxPool: int = 5) -> None:
    if DBConnectionPool.__maxPool != maxPool:
      DBConnectionPool.__maxPool = maxPool
      print(f'Database connections maximum pool size set to {maxPool}')

  @staticmethod
  def getInstance():
    if not DBConnectionPool.__instance:
      DBConnectionPool.__instantiationBlock = False
      DBConnectionPool()
      DBConnectionPool.__instantiationBlock = True
    return DBConnectionPool.__instance
