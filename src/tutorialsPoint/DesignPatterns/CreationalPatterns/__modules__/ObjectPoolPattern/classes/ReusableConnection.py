class ReusableConnection():
  def __init__(self, connectionsPool):
    self.__connectionsPool = connectionsPool
    self.__connectionDetails = {}
    self.__connect = False

  def lock(self) -> None:
    self.__connectionsPool.lock(self)

  def unlock(self) -> None:
    self.__connectionsPool.unlock(self)

  def connect(self, **kwargs) -> dict:
    self.__connectionDetails = kwargs
    if self.__connectionDetails["host"] != "127.0.0.1":
      return {
        "success": False,
        "reason": f'{self.__connectionDetails["database"]}'\
            f'@{self.__connectionDetails["host"]} declined connection for '\
            f'{self.__connectionDetails["username"]}'
      }
    self.__connect = True
    print(
      f'Connection to {self.__connectionDetails["database"]}'\
      f'@{self.__connectionDetails["host"]}?'\
      f'u={self.__connectionDetails["username"]}'
      f'&p={"*" * len(self.__connectionDetails["password"])} established.'
    )
    return {
      "success": True,
    }
  
  def close(self) -> None:
    self.__connect = False
    print(
      f'Connection to {self.__connectionDetails["database"]}'\
      f'@{self.__connectionDetails["host"]}?'\
      f'u={self.__connectionDetails["username"]}'
      f'&p={"*" * len(self.__connectionDetails["password"])} closed.'
    )

  def reset(self) -> None:
    self.__connectionDetails = {}

  def returnToPool(self) -> None:
    if self.__connect:
      self.close()
    self.reset()
    self.unlock()