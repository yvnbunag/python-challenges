from . import SingletonShapeStore

class DatabaseRequester():
  def __init__(self):
    shapeStore = SingletonShapeStore.getInstance()
    self.__requestCache = {}
    self.__mockDatabase = {
      "1": shapeStore.getShape("HeavyCircle"),
      "2": shapeStore.getShape("HeavyRectangle")
    }

  def get(self, ID):
    # If present in cache, return Prototype Clone
    if ID in self.__requestCache:
      print(f'DB::Object with ID = {ID} cloned from prototype cache')
      return self.__requestCache[ID].clone()

    # Mock database get operation
    databaseObject = self.__mockDatabase[ID]
    # Put object prototype to cache
    self.__requestCache[ID] = databaseObject.clone()

    print(
      f'DB::Object with ID = {ID} fetched from database and stored to ' \
      'prototype cache'
    )

    # Return object prototype clone
    return self.__requestCache[ID].clone()

  def update(self, ID, newValue):
    # Mock database update operation
    self.__mockDatabase[ID] = newValue.clone()

    print(
      f'DB::Object with ID = {ID} updated on the database and stored in ' \
      'prototype cache'
    )
    # If present, update prototype value in cache
    if ID in self.__requestCache:    
      self.__requestCache[ID] = self.__mockDatabase[ID].clone()
  
    return self.get(ID)