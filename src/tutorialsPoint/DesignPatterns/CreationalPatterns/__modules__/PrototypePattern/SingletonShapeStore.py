from .classes import HeavyCircle, HeavyRectangle, HeavySquare

class SingletonShapeStore():
  __shapeMap = {}
  __instance = None
  __initializationBlock = True

  def __init__(self):
    if SingletonShapeStore.__initializationBlock:
      raise Exception(
        "Shape store is a singleton class and an instance can only be "\
        "accessed using getInstance() method"
      )
    else:
      SingletonShapeStore.__shapeMap["HeavyCircle"] = HeavyCircle()
      SingletonShapeStore.__shapeMap["HeavyRectangle"] = HeavyRectangle()
      SingletonShapeStore.__shapeMap["HeavySquare"] = HeavySquare()
      SingletonShapeStore.__instance = self
  
  def getShape(self, shape):
    if self.__shapeMap[shape]:
      return self.__shapeMap[shape].clone()
    raise Exception("Shape does not exist")
  
  def getPrototypeAddress(self, shape):
    return hex(id(self.__shapeMap[shape]))

  @staticmethod
  def getInstance():
    if SingletonShapeStore.__instance == None:
      SingletonShapeStore.__initializationBlock = False
      SingletonShapeStore()
      SingletonShapeStore.__initializationBlock = True
    return SingletonShapeStore.__instance

