from copy import deepcopy
from abc import ABC, abstractmethod

class HeavyShape(ABC):
  @abstractmethod
  def getShape(self):
    pass

  def clone(self):
    return deepcopy(self)

