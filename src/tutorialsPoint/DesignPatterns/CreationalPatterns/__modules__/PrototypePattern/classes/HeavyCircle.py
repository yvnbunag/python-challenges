from ..abstract import HeavyShape

class HeavyCircle(HeavyShape):
  def getShape(self):
    return "Heavy Circle"