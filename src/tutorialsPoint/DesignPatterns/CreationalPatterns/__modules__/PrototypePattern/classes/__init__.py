from .HeavyCircle import HeavyCircle
from .HeavyDatabaseObject import HeavyDatabaseObject
from .HeavyRectangle import HeavyRectangle
from .HeavySquare import HeavySquare