from ..abstract import HeavyShape

class HeavyRectangle(HeavyShape):
  def getShape(self):
    return "Heavy Rectangle"