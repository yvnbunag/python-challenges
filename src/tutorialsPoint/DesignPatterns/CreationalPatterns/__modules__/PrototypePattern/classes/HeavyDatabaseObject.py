from interface import implements
from ..interfaces import HeavyDatabaseObjectInterface

class HeavyDatabaseObject(implements(HeavyDatabaseObjectInterface)):
  def __init__(self, id, objectValue):
    self.__ID = ID
    self.__objectValue = objectValue
  
  def getID(self):
    return self.__ID

  def getValue(self):
    return self.__objectValue

  def changeValue(self, objectValue):
    self.__objectValue = objectValue
    return True