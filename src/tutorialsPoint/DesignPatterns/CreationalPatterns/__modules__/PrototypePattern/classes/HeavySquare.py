from ..abstract import HeavyShape

class HeavySquare(HeavyShape):
  def getShape(self):
    return "Heavy Square"