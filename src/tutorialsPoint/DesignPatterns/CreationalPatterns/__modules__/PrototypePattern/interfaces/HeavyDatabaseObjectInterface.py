from interface import Interface

class HeavyDatabaseObjectInterface(Interface):
  def __init__(self, id, objectValue):
    pass
  
  def getID(self):
    pass

  def getValue(self):
    pass

  def changeValue(self, objectValue):
    pass