from interface import implements
from ..interfaces import Shape

class Rectangle(implements(Shape)):
  def draw(self) -> None:
    print("Rectangle class instance's draw() method called.")