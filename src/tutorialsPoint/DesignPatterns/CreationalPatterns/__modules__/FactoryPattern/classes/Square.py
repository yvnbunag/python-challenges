from interface import implements
from ..interfaces import Shape

class Square(implements(Shape)):
  def draw(self) -> None:
    print("Square class instance's draw() method called.")