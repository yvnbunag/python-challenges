from .classes import Circle, Rectangle, Square
from .interfaces import Shape
class ShapeFactory():
  def getShape(self, shape: str) -> Shape:
    if shape.lower() == 'circle':
      return Circle()
    if shape.lower() == 'rectangle':
      return Rectangle()
    if shape.lower() == 'square':
      return Square()
    return None