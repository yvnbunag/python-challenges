from interface import Interface

class Shape(Interface):
  def draw(self) -> None:
    pass