from interface import implements
from ..interfaces import Shape

class RoundedRectangle(implements(Shape)):
  def draw(self) -> None:
    print("RoundedRectangle class instance's draw() method called.")