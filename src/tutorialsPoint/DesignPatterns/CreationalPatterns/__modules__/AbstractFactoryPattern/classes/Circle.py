from interface import implements
from ..interfaces import Shape

class Circle(implements(Shape)):
  def draw(self) -> None:
    print("Circle class instance's draw() method called.")