from interface import implements
from ..interfaces import Shape

class RoundedSquare(implements(Shape)):
  def draw(self) -> None:
    print("RoundedSquare class instance's draw() method called.")