from ..abstract import AbstractShapeFactory
from ..interfaces import Shape
from ..classes import RoundedRectangle, RoundedSquare

class RoundedShapeFactory(AbstractShapeFactory):
  def getShape(self, shape) -> Shape:
    if shape.lower() == "rectangle":
      return RoundedRectangle()
    if shape.lower() == "square":
      return RoundedSquare()
    return None