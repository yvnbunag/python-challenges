from ..abstract import AbstractShapeFactory
from ..interfaces import Shape
from ..classes import Circle, Rectangle, Square

class ShapeFactory(AbstractShapeFactory):
  def getShape(self, shape) -> Shape:
    if shape.lower() == "circle":
      return Circle()
    if shape.lower() == "rectangle":
      return Rectangle()
    if shape.lower() == "square":
      return Square()
    return None