from abc import ABC, abstractmethod
from ..interfaces import Shape

class AbstractShapeFactory(ABC):
  @abstractmethod
  def getShape(self, shape: str) -> Shape:
    pass