from .factory import RoundedShapeFactory, ShapeFactory
from .abstract import AbstractShapeFactory

class FactoryProducer():
  @staticmethod
  def getFactory(factory: str = "normal") -> AbstractShapeFactory:
    if factory.lower() == "rounded":
      return RoundedShapeFactory()
    else:
      return ShapeFactory()