class EventLogger():
  __instance = None
  __instantiationBlock = True

  def __init__(self):
    """ Virtually private constructor. """
    if EventLogger.__instantiationBlock:
        raise Exception(
          "Singleton:: EventLogger instance can only be accessed thru getInstance()"
        )
    else:
        EventLogger.__instance = self

  def log(self, log: str) -> None:
    print(f'EventLogger singleton class logged "{log}"')
    
  @staticmethod 
  def getInstance():
    """ Static access method. """
    if EventLogger.__instance == None:
      EventLogger.__instantiationBlock = False
      EventLogger()
      EventLogger.__instantiationBlock = True
    return EventLogger.__instance
    