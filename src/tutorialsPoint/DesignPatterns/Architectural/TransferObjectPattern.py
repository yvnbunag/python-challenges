from src.__modules__ import challenge
from .__modules__.TransferObjectPattern import StudentsDatabase
from .__modules__.TransferObjectPattern.classes import Student

class TransferObjectPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/transfer_object_pattern.htm"
    self.challengeDescription = "The Transfer Object pattern is used when we "\
      "want to pass data with multiple attributes in one shot from client to "\
      "server. Transfer object is also known as Value Object. Transfer Object "\
      "is a simple POJO class having getter/setter methods and is "\
      "serializable so that it can be transferred over the network. It does "\
      "not have any behavior. Server Side business class normally fetches "\
      "data from the database and fills the POJO and send it to the client "\
      "or pass it by value. For client, transfer object is read-only. Client "\
      "can create its own transfer object and pass it to server to update "\
      "values in database in one shot. Following are the entities of this "\
      "type of design pattern."\
      "\n Business Object - Business Service fills the Transfer Object with "\
      "data."\
      "\n Transfer Object - Simple POJO having methods to set/get attributes "\
      "only."\
      "\n Client - Client either requests or sends the Transfer Object to "\
      "Business Object."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    studentsDatabase = StudentsDatabase()
    
    students = studentsDatabase.getAll()

    print("Students in the database:")
    for student in students:
      print()
      print("Name:", student.getName())
      print("ID:", student.getID())
      print("Year level:", student.getYearLevel())
    
    studentToModify = studentsDatabase.get("MCKSTD2")
    studentToModify.setName(studentToModify.getName() + "MODIFIED")
    studentToModify.setYearLevel("4")
    studentsDatabase.update(studentToModify)

    updatedStudent = studentsDatabase.get("MCKSTD2")
    print("\nUpdated student with ID = MCKSTD2")
    print("Name:", updatedStudent.getName())
    print("ID:", updatedStudent.getID())
    print("Year level:", updatedStudent.getYearLevel())

    newStudent = Student("NewStudent1", "MCKSTD3", "2")
    studentsDatabase.create(newStudent)

    print("\nInserting student to database with ID = MCKSTD3")

    students = studentsDatabase.getAll()

    print("New list of students in the database:")
    for student in students:
      print()
      print("Name:", student.getName())
      print("ID:", student.getID())
      print("Year level:", student.getYearLevel())


if __name__ == "__main__":
  main = TransferObjectPattern()
  main.execute()