from abc import ABC, abstractmethod

class Service(ABC):
  def getName(self) -> str:
    return self.__class__.__name__
  
  @abstractmethod
  def execute():
    pass

  @abstractmethod
  def getOperation():
    pass