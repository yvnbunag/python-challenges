from importlib import import_module
from .abstract import Service
import os
import sys

class ServiceContext():
  def __init__(self):
    # Mapping from external source
    self.__servicesMapping = {
      "Add": ".classes",
      "Divide": ".classes",
      "Multiply": ".classes",
      "Subtract": ".classes"
    }

  def lookup(self, serviceName: str) -> Service:
    path, filename = os.path.split(os.path.realpath(__file__))
    currentPath = path.replace(f'{os.getcwd()}\\', "").replace("\\", ".")

    if serviceName in self.__servicesMapping:
      NewService = getattr(
        import_module(
          f'{currentPath}{self.__servicesMapping[serviceName]}'
        ), serviceName
      )
      return NewService()
    return False
