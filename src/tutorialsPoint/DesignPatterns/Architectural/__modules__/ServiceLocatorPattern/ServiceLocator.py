from .ServiceCache import ServiceCache
from .ServiceContext import ServiceContext
from .abstract import Service

class ServiceLocator():
  def __init__(self):
    self.__serviceCache = ServiceCache()
    self.__ServiceContext = ServiceContext()
  
  def getService(self, serviceName: str) -> Service:
    cachedService = self.__serviceCache.getService(serviceName)
    if cachedService:
      return cachedService

    newService = self.__ServiceContext.lookup(serviceName)
    if newService:
      self.__serviceCache.cacheService(newService)
      return newService

    return False

