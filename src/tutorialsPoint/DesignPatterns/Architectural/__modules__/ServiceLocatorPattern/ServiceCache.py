from .abstract import Service

class ServiceCache():
  def __init__(self):
    self.__services = {}
  
  def getService(self, serviceName: str) -> Service:
    if serviceName in self.__services:
      print(f'CACHE::{serviceName} class instance obtained from cache')
      return self.__services[serviceName]
    return False
  
  def cacheService(self, service: Service) -> None:
    print(f'CACHE::{service.getName()} class instance cached from new instance')
    self.__services[service.getName()] = service