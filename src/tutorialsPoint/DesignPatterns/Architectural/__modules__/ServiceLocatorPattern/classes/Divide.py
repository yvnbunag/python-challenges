from ..abstract import Service

class Divide(Service):
  def execute(self, num1: int, num2: int) -> float:
    return num1 / num2

  def getOperation(self) -> str:
    return "/"