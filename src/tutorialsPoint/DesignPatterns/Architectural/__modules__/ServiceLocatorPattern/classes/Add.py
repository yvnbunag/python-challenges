from ..abstract import Service

class Add(Service):
  def execute(self, num1: int, num2: int) -> int:
    return num1 + num2
  
  def getOperation(self) -> str:
    return "+"