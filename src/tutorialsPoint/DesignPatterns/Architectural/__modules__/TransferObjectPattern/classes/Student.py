class Student():
  def __init__(self, name: str = None, ID: str = None, yearLevel: str = None):
    self.__name = name
    self.__ID = ID
    self.__yearLevel = yearLevel

  def getName(self) -> str:
    return self.__name
  
  def getID(self) -> int:
    return self.__ID

  def getYearLevel(self) -> str:
    return self.__yearLevel
  
  def setName(self, name: str = None) -> None:
    if name:
      self.__name = name

  def setID(self, ID: str = None) -> None:
    if ID:
      self.__ID = ID
  
  def setYearLevel(self, yearLevel: str = None) -> None:
    if yearLevel:
      self.__yearLevel = yearLevel

  def serialize(self) -> dict:
    return {
      "name": self.__name,
      "id": self.__ID,
      "yearLevel": self.__yearLevel
    }

  def deserialize(self, serializedStudent: dict) -> None:
    self.__name = serializedStudent["name"]
    self.__ID = serializedStudent["id"]
    self.__yearLevel = serializedStudent["yearLevel"]