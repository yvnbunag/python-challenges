from .classes import Student

# Mock students database
class StudentsDatabase():
  def __init__(self):
    self.__students = {}
    self.__students["MCKSTD1"] = {
      "name": "MockStudent1",
      "id": "MCKSTD1",
      "yearLevel": "1"
    }

    self.__students["MCKSTD2"] = {
      "name": "MockStudent2",
      "id": "MCKSTD2",
      "yearLevel": "3"
    }
  
  def get(self, ID: str) -> Student:
    if ID in self.__students:
      student = Student()
      student.deserialize(self.__students[ID])
      return student
    return False

  def getAll(self) -> list:
    studentList = []
    for index, student in self.__students.items():
      desStudent = Student()
      desStudent.deserialize(student)
      studentList.append(desStudent)
    
    return studentList

  def update(self, student: Student) -> None:
    serializedStudent = student.serialize()
    self.__students[student.getID()] = serializedStudent
  
  def create(self, student: Student) -> None:
    serializedStudent = student.serialize()
    self.__students[student.getID()] = serializedStudent