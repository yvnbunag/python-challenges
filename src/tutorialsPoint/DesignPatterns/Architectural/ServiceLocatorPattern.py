from src.__modules__ import challenge
from .__modules__.ServiceLocatorPattern import ServiceLocator

class ServiceLocatorPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/service_locator_pattern.htm"
    self.challengeDescription = "The service locator design pattern is used "\
      "when we want to locate various services using JNDI lookup. Considering "\
      "high cost of looking up JNDI for a service, Service Locator pattern "\
      "makes use of caching technique. For the first time a service is "\
      "required, Service Locator looks up in JNDI and caches the service "\
      "object. Further lookup or same service via Service Locator is done in "\
      "its cache which improves the performance of application to great "\
      "extent. Following are the entities of this type of design pattern."\
      "\n Service - Actual Service which will process the request. Reference "\
      "of such service is to be looked upon in JNDI server."\
      "\n Context / Initial Context - JNDI Context carries the reference to "\
      "service used for lookup purpose."\
      "\n Service Locator - Service Locator is a single point of contact to "\
      "get services by JNDI lookup caching the services."\
      "\n Cache - Cache to store references of services to reuse them"\
      "\n Client - Client is the object that invokes the services via "\
      "ServiceLocator."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    serviceList = [
      "Add",
      "Subtract",
      "Multiply",
      "Divide",
      "Modulo"
    ]
    executionVariables = [
      [10, 2],
      [30, 15],
      [40, 8]
    ]
    serviceLocator = ServiceLocator()

    for num1, num2 in executionVariables:
      for serviceName in serviceList:
        service  = serviceLocator.getService(serviceName)

        if not service:
          print(f'{serviceName} service does not exist\n')
          break

        print(
          f'{service.getName()}() memory address = {hex(id(service))}'
        )
        print(
          f'{service.getName()}():: {num1} {service.getOperation()} {num2} =',
          service.execute(num1, num2)
        )
        print()
        
if __name__ == "__main__":
  main = ServiceLocatorPattern()
  main.execute()