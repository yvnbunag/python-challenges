from src.__modules__ import challenge
from .__modules__.ProxyPattern.classes import InternetEUProxy, InternetUSProxy
class ProxyPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/proxy_pattern.htm"
    self.challengeDescription = "In proxy pattern, a class represents "\
      "functionality of another class. This type of design pattern comes "\
      "under structural pattern."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    euInternet = InternetEUProxy()
    usInternet = InternetUSProxy()

    euRequests = [
      "facebook.com",
      "illegal-in-eu.com",
      "illegal-in-us.com",
      "google.com",
      "some-illegal-website.com",
      "website-in-deep-web.com"
    ]

    usRequests = [
      "facebook.com",
      "black-market.com",
      "against-the-law.com",
      "google.com",
      "some-illegal-website.com",
      "website-in-deep-web.com"
    ]

    print("EU connection requests")
    for request in euRequests:
      connection = euInternet.connect(request)
      if connection["success"]:
        print(f'EU connection to https://www.{request} successful')
        continue
      print(
        f'EU connection to https://www.{request} blocked from '\
        f'{connection["blockedFrom"]}'
      )

    print("\nUS connection requests")
    for request in usRequests:
      connection = usInternet.connect(request)
      if connection["success"]:
        print(f'US connection to https://www.{request} successful')
        continue
      print(
        f'US connection to https://www.{request} blocked from '\
        f'{connection["blockedFrom"]}'
      )
if __name__ == "__main__":
  main = ProxyPattern()
  main.execute()