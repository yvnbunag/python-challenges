from src.__modules__ import challenge
from .__modules__.FacadePattern import OnlineOrderAPI

class FacadePattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "value"
    self.challengeDescription = "Facade pattern hides the complexities of the "\
      "system and provides an interface to the client using which the client "\
      "can access the system. This type of design pattern comes under "\
      "structural pattern as this pattern adds an interface to existing "\
      "system to hide its complexities."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    processOrder = lambda a, b, c: a + b + c
    mockOrderID = processOrder("mock", "Order", "002")
    onlineOrderFacade = OnlineOrderAPI()
    try:
      deliveryDetails = onlineOrderFacade.process(mockOrderID)
      print(
        "Order successfull! Package will now be delivered. Details are as "\
        "follows:"
      )
      for k, v in deliveryDetails.items():
        print(f'{k}: {v}')
    except Exception as err:
      print(err)

if __name__ == "__main__":
  main = FacadePattern()
  main.execute()