from src.__modules__ import challenge, levelPrint
from .__modules__.FlyweightPattern import ParticleFactory
from .__modules__.FlyweightPattern.classes import Projectile, Canvas

class FlyweightPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/flyweight_pattern.htm"
    self.challengeDescription = "Flyweight pattern tries to reuse already "\
      "existing similar kind objects by storing them and creates new object "\
      "when no matching object is found."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    print("<<Bullet objects flyweight design pattern demonstration>>")
    levelPrint(1, "Legend: ")
    levelPrint(2, "= Misile Projectile, speed = 1")
    levelPrint(2, "~ Bullet Projectile, speed = 3")
    levelPrint(2, "- Rifle Bullet Projectile, speed = 5")
    print()
    # Particle Objects are referenced to a single location and are immutable
    particleFactory = ParticleFactory()
    canvas = Canvas.newCanvas()

    # Projectile objects point to a specific particle location to be displayed
    projectileStore = [
      Projectile(
        particleFactory.getParticle("missile"),
        0,
        "right"
      ),
      Projectile(
        particleFactory.getParticle("bullet"),
        0,
        "right"
      ),
      Projectile(
        particleFactory.getParticle("bullet"),
        10,
        "right"
      ),
      Projectile(
        particleFactory.getParticle("rifleBullet"),
        0,
        "right"
      )
    ]

    iterations = 51
    ctr = 1
    while ctr <= iterations:
      # AP = Alive Projectiles or Projectiles stored in projectilesStore
      print(f'Frame {ctr}, AP = {len(projectileStore)}:', end = "\t")
      for index, projectile in enumerate(projectileStore):
        # Draw projectile in canvas with its particle
        projectile.draw(canvas)
        # Move projectile to its direction based on its particle speed
        projectile.move()
        # Delete projectile if it exceeds canvas boundaries
        projectile.checkLife(canvas, projectileStore, index)
      # Print the canvas
      Canvas.display(canvas)
      canvas = Canvas.newCanvas()
      ctr += 1

if __name__ == "__main__":
  main = FlyweightPattern()
  main.execute()