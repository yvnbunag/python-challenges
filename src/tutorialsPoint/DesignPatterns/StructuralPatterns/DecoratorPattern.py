from src.__modules__ import challenge
from .__modules__.DecoratorPattern.abstract import ShapeColorDecorator
from .__modules__.DecoratorPattern.interfaces import Shape
from .__modules__.DecoratorPattern.classes import *
from .__modules__.DecoratorPattern import methodDecorator

class DecoratorPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/decorator_pattern.htm"
    self.challengeDescription = "Decorator pattern allows a user to add new "\
      "functionality to an existing object without altering its structure. "\
      "This type of design pattern comes under structural pattern as this "\
      "pattern acts as a wrapper to existing class."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    print("<<Class Decorator Example>>")

    rectangle = Rectangle(1, 2)
    square = Square(10)
    # In decorator instantiation, the object to be decorated is instantiated
    # into the decorator class
    redRectangle = RedShapeDecorator(
      Rectangle(1, 2)
    )
    greenSquare = GreenShapeDecorator(
      Square(10)
    )

    executionList = [
      rectangle,
      square,
      redRectangle,
      greenSquare
    ]

    for shape in executionList:
      print()
      print(shape.draw(), " drawn")
      print(
        "Area: ",
        f'{"{0:.2f}".format(shape.getArea())}{shape.unit()}'
      )
    
    print("\n<<Function Decorator Example>>")
    print("heavyMessageFunction() function called, please wait...")
    print(
      methodDecorator.heavyMessageFunction("Heavy function returned this.")
    )
    

if __name__ == "__main__":
  main = DecoratorPattern()
  main.execute()