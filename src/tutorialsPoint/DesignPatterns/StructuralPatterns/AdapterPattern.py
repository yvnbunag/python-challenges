from src.__modules__ import challenge
from .__modules__.AdapterPattern import ColorFactory, OldColorFactory
from .__modules__.AdapterPattern.classes import OldColorAdapter

class AdapterPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/adapter_pattern.htm"
    self.challengeDescription = "This pattern involves a single class which "\
      "is responsible to join functionalities of independent or incompatible "\
      "interfaces. A real life example could be a case of card reader which "\
      "acts as an adapter between memory card and a laptop. You plugin the "\
      "memory card into card reader and card reader into the laptop so that "\
      "memory card can be read via laptop."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    colorObjects = {}
    colorFactory = ColorFactory()
    oldColorFactory = OldColorFactory()

    colorObjects["Red"] = colorFactory.getColor("red")
    # In adapter instantiation, the object to attach the adapter with is
    # instantiated into the decorator class
    colorObjects["Old Red"] = OldColorAdapter(
      oldColorFactory.getColor("red")
    )

    colorObjects["Green"] = colorFactory.getColor("green")
    colorObjects["Old Green"] = OldColorAdapter(
      oldColorFactory.getColor("green")
    )

    colorObjects["Blue"] = colorFactory.getColor("blue")
    colorObjects["Old Blue"] = OldColorAdapter(
      oldColorFactory.getColor("blue")
    )

    colorObjects["Old Red No Adapter"] = oldColorFactory.getColor("red")

    for key, color in colorObjects.items():
      try:
        rgb = color.getColorRGB()
        print(
          f'{key} object result of getColorRGB() method being called: {rgb}'
        )

        hex = color.getColorHex()
        print(
          f'{key} object result of getColorHex() method being called: {hex}'
        )
      except:
        print(
          f'{key} object does not implement getColorRGB() and getColorHex() methods'
        )
      print()



if __name__ == "__main__":
  main = AdapterPattern()
  main.execute()