from .AdapterPattern import AdapterPattern
from .BridgePattern import BridgePattern
from .CompositePattern import CompositePattern
from .DecoratorPattern import DecoratorPattern
from .FacadePattern import FacadePattern
from .FlyweightPattern import FlyweightPattern
from .FrontControllerPattern import FrontControllerPattern
from .ProxyPattern import ProxyPattern