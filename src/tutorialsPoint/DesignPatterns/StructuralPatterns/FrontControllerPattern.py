from src.__modules__ import challenge
from .__modules__.FrontControllerPattern import FrontController
class FrontControllerPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/front_controller_pattern.htm"
    self.challengeDescription = "The front controller design pattern is used "\
      "to provide a centralized request handling mechanism so that all "\
      "requests will be handled by a single handler. This handler can do the "\
      "authentication/ authorization/ logging or tracking of request and then "\
      "pass the requests to corresponding handlers. Following are the "\
      "entities of this type of design pattern."\
      " \nFront Controller - Single handler for all kinds of requests coming "\
      "to the application (either web based/ desktop based)."\
      " \nDispatcher - Front Controller may use a dispatcher object which can "\
      "dispatch the request to corresponding specific handler."\
      " \nView - Views are the object for which the requests are made."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    requests = [
      {"/home": "some:valid:jwt"},
      {"/about": "some:valid:jwt"},
      {"/home": "is-a:valid:jwt"},
      {"/about": "some:invalid:jwt"}
    ]
    frontController = FrontController()

    for request in requests:
      URL, JWT = next(iter(request.items()))
      print(f'\nINCOMING REQUEST:: {URL} access request from JWT = {JWT}')
      print(f'OUTGOING RESPONSE:: {frontController.dispatchRequest(JWT, URL)}')
      

if __name__ == "__main__":
  main = FrontControllerPattern()
  main.execute()