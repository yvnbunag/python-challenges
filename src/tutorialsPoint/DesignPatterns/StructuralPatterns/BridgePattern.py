from src.__modules__ import challenge
from .__modules__.BridgePattern.classes import *

class BridgePattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/bridge_pattern.htm"
    self.challengeDescription = "Bridge is used when we need to decouple an "\
      "abstraction from its implementation so that the two can vary "\
      "independently. This type of design pattern comes under structural "\
      "pattern as this pattern decouples implementation class and abstract "\
      "class by providing a bridge structure between them. "\
      "This pattern involves an interface which acts as a bridge which makes "\
      "the functionality of concrete classes independent from interface "\
      "implementer classes. Both types of classes can be altered structurally "\
      "without affecting each other."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    redCircle = Circle(1, 1, 1, RedCircle())
    redCircle.draw()
    greenCircle = Circle(2, 2, 2, GreenCircle())
    greenCircle.draw()
    blueCircle = Circle(3, 3, 3, BlueCircle())
    blueCircle.draw()

if __name__ == "__main__":
  main = BridgePattern()
  main.execute()