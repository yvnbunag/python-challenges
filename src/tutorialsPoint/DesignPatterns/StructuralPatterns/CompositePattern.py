from src.__modules__ import challenge, levelPrint
from .__modules__.CompositePattern.classes import Employee
from ..CreationalPatterns.__modules__.BuilderPattern import *

class CompositePattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/composite_pattern.htm"
    self.challengeDescription = "Composite pattern is used where we need to "\
      "treat a group of objects in similar way as a single object. Composite "\
      "pattern composes objects in term of a tree structure to represent part "\
      "as well as whole hierarchy. This type of design pattern comes under "\
      "structural pattern as this pattern creates a tree structure of group "\
      "of objects."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    print("<<Employee System Example>>")

    clerk1 = Employee("Laura", "Marketing")
    clerk2 = Employee("Bob", "Marketing")

    salesExecutive1 = Employee("Richard", "Sales")
    salesExecutive2 = Employee("Rob", "Sales")
    salesExecutive3 = Employee("Gina", "Sales")

    headMarketing = Employee(
      "Michael",
      "Head Marketing",
      20000.00,
      [clerk1, clerk2]
    )

    headSales = Employee(
      "Robert",
      "Head Sales",
      20000.00,
      [salesExecutive1, salesExecutive2]
    )

    headSales.addSubordinate(salesExecutive3)
    headSales.removeSubordinate(salesExecutive1)

    ceo = Employee(
      "John",
      "CEO",
      30000.00,
      [headMarketing, headSales]
    )

    Employee.getHierarchy(ceo)

    print("\n<<HTML DOM Example>>")

    elementsFactory = HTMLElementsFactory()
    
    title = elementsFactory.create("title").setValue("Composite HTML Structure")
    head = elementsFactory.create("head").addChild(title)


    divSpan = elementsFactory.create("span").setValue("Composite Title")
    div = elementsFactory.create("div").addChild(divSpan)
    subSpan = elementsFactory.create("span").setValue("Composite Sub Title")

    body = elementsFactory.create("body").addChild(div).addChild(subSpan)
    root = elementsFactory.create("html").addChild(title).addChild(body)

    self.htmlCompositeIterator(root)

  def htmlCompositeIterator(self, root, iteration:int = 0):
    iteration
    levelPrint(iteration, f'<{root.getName()}>')
    if root.getValue():
      levelPrint(iteration + 2, root.getValue())
    if root.getChildren():
      for children in root.getChildren():
        self.htmlCompositeIterator(children, iteration + 2)
    if root.endTag():
      levelPrint(iteration, f'</{root.getName()}>')

if __name__ == "__main__":
  main = CompositePattern()
  main.execute()