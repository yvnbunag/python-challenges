from abc import ABC, abstractmethod
from interface import implements
from ..interfaces import Shape
from math import sqrt
# Abstract method (multiple inheritance conflict with interface.implements())
class ShapeColorDecorator(implements(Shape)):
  def __init__(self, shape: Shape):
    self.shape = shape

  def draw(self) -> str:
    pass

  def getArea(self) -> float:
    return (sqrt(self.shape.getArea()) * 2.54) ** 2

  def unit(self) -> str:
    return "cm."