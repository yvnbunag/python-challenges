import time

def calculateTime(function):
  def decoratorCall(*args, **kwargs):
    start = time.time()
    returnValue = function(*args, **kwargs)
    end = time.time()

    print(f'Total time taken in {function.__name__}(): {end - start}')
    return returnValue
  
  return decoratorCall

@calculateTime
def heavyMessageFunction(message: str):
  time.sleep(2)
  return message