from interface import Interface

class Shape(Interface):
  def draw(self) -> str:
    pass
  def getArea(self) -> float:
    pass
  def unit(self) -> str:
    pass