from .GreenShapeDecorator import GreenShapeDecorator
from .Rectangle import Rectangle
from .RedShapeDecorator import RedShapeDecorator
from .Square import Square