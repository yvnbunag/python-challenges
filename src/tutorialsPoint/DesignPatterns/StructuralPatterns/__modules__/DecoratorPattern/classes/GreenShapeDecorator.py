from ..abstract import ShapeColorDecorator
from ..interfaces import Shape

class GreenShapeDecorator(ShapeColorDecorator):
  def __init__(self, shape: Shape):
    super().__init__(shape)

  def draw(self) -> str:
    return f'Green {self.shape.draw()}'