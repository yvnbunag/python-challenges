from ..abstract import ShapeColorDecorator
from ..interfaces import Shape

class RedShapeDecorator(ShapeColorDecorator):
  def __init__(self, shape: Shape):
    super().__init__(shape)
    
  def draw(self) -> str:
    return f'Red {self.shape.draw()}'