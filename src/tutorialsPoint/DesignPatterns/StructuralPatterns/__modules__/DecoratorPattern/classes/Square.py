from interface import implements
from ..interfaces import Shape

class Square(implements(Shape)):
  def __init__(self, sides: float):
    self.__sides = float(sides)

  def draw(self) -> str:
    return "Square"
  
  def getArea(self) -> float:
    return self.__sides ** 2
  
  def unit(self) -> str:
    return "in."