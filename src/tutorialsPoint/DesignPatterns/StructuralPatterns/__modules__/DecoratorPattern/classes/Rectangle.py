from interface import implements
from ..interfaces import Shape

class Rectangle(implements(Shape)):
  def __init__(self, height: float, width: float):
    self.__height = float(height)
    self.__width = float(width)

  def draw(self) -> str:
    return "Rectangle"
  
  def getArea(self) -> float:
    return self.__height * self.__width
  
  def unit(self) -> str:
    return "in."