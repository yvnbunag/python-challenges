from interface import implements
from ..interfaces import Internet
from . import InternetGlobalProxy

class InternetUSProxy(implements(Internet)):
  def __init__(self):
    self.__internet = InternetGlobalProxy()
    self.__blacklist = [
      "illegal-in-us.com",
      "against-the-law.com",
      "some-illegal-website.com"
    ]

  def connect(self, URL) -> dict:
    if URL in self.__blacklist:
      return {
        "success": False,
        "blockedFrom": "US-Proxy-Server"
      }
    return self.__internet.connect(URL)