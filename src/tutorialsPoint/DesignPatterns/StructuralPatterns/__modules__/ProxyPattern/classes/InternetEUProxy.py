from interface import implements
from ..interfaces import Internet
from . import InternetGlobalProxy

class InternetEUProxy(implements(Internet)):
  def __init__(self):
    self.__internet = InternetGlobalProxy()
    self.__blacklist = [
      "illegal-in-eu.com",
      "black-market.com"
    ]

  def connect(self, URL) -> dict:
    if URL in self.__blacklist:
      return {
        "success": False,
        "blockedFrom": "EU-Proxy-Server"
      }
    return self.__internet.connect(URL)