from interface import implements
from ..interfaces import Internet

class RealInternet(implements(Internet)):
  def connect(self, URL) -> dict:
    return {
      "success": True
    }