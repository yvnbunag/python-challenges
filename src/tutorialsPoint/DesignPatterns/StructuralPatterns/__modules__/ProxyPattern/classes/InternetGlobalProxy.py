from interface import implements
from ..interfaces import Internet
from . import RealInternet

class InternetGlobalProxy(implements(Internet)):
  def __init__(self):
    self.__internet = RealInternet()
    self.__blacklist = [
      "some-illegal-website.com",
      "website-in-deep-web.com"
    ]

  def connect(self, URL) -> dict:
    if URL in self.__blacklist:
      return {
        "success": False,
        "blockedFrom": "GLOBAL-Proxy-Server"
      }
    return self.__internet.connect(URL)