from .classes.controllers import *

class Dispatcher():
  def __init__(self):
    pass

  def dispatch(self, URL: str) -> dict:
    return {
      "/home": HomeController().index(),
      "/about": AboutController().index()
    }[URL]