class JWTParser():
  # Mock JWT Parser class
  def __init__(self):
    self.mockJWTValues = {
      "some:valid:jwt": {
        "id": 1,
        "name": "mockNameAA"
      },
      "is-a:valid:jwt": {
        "id": 2,
        "name": "mockNameBB"
      }
    }
  
  def parse(self, jwt: str) -> dict:
    if jwt in self.mockJWTValues:
      return self.mockJWTValues[jwt]
    return {
      "id": 3,
      "name": "invalidMockName"
    }