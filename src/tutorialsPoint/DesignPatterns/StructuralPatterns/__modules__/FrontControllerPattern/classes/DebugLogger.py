from ..abstract import ChainLogger

class DebugLogger(ChainLogger):
  def log(self, level: int, message: str) -> None:
    if self.level <= level:
      print("DEBUG LOG:", message)
      if self.nextLogger:
        self.nextLogger.log(level, message)