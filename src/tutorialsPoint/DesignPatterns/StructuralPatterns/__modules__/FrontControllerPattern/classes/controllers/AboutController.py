from interface import implements
from ...interfaces import Controller

class AboutController(implements(Controller)):
  def index(self):
    return {
      "success": True,
      "data": "About Page"
    }