from interface import implements
from ...interfaces import Controller

class HomeController(implements(Controller)):
  def index(self):
    return {
      "success": True,
      "data": "Home Page"
    }