from ..abstract import ChainLogger

class ErrorLogger(ChainLogger):
  def log(self, level: int, message: str) -> None:
    if self.level <= level:
      print("ERROR LOG:", message)
      if self.nextLogger:
        self.nextLogger.log(level, message)