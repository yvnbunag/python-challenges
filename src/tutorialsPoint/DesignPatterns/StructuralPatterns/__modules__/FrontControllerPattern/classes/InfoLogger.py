from ..abstract import ChainLogger

class InfoLogger(ChainLogger):
  def log(self, level: int, message: str) -> None:
    if self.level <= level:
      print("INFO LOG:", message)
      if self.nextLogger:
        self.nextLogger.log(level, message)