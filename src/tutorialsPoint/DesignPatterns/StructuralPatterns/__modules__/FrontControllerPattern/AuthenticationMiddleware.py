class AuthenticationMiddleware():
  # Mock authentication middleware class
  def __init__(self):
    self.authenticatedUsers = [1, 2]
  
  def validate(self, jwtObject: dict) -> None:
    if jwtObject["id"] not in self.authenticatedUsers:
      exceptionMessage = f'User with ID = {jwtObject["id"]} and Name = '\
        f'{jwtObject["name"]} request unauthenticated'
      raise Exception(exceptionMessage)