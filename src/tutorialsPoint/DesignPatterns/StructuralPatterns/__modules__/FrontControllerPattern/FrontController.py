from .AuthenticationMiddleware import AuthenticationMiddleware
from .Dispatcher import Dispatcher
from .JWTParser import JWTParser
from .RequestLogger import RequestLogger
from .abstract import ChainLogger

class FrontController():
  def __init__(self):
    self.authenticationMiddleware = AuthenticationMiddleware()
    self.jwtParser = JWTParser()
    self.dispatcher = Dispatcher()
    self.requestLogger = RequestLogger.getInstance()

  def dispatchRequest(self, JWT: str, URL: str) -> dict:
    try:
      jwtObject = self.jwtParser.parse(JWT)
      self.authenticationMiddleware.validate(jwtObject)
      response = self.dispatcher.dispatch(URL)

      self.requestLogger.log(
        ChainLogger.INFO,
        f'User with ID = {jwtObject["id"]} accessed {URL}'
      )

      return response

    except Exception as err:
      self.requestLogger.log(ChainLogger.ERROR, err)
      return {
        "success": False,
        "reason": "unauthenticated"
      }