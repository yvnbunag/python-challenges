from abc import ABC, abstractmethod

class ChainLogger(ABC):
  INFO = 1
  DEBUG = 2
  ERROR = 3

  def __init__(self, level: int = 1):
    self.level = level
    self.nextLogger = None
  
  def setNextLogger(self, logger) -> None:
    self.nextLogger = logger

  @abstractmethod
  def log(self, level: int, message: str) -> None:
    pass

    