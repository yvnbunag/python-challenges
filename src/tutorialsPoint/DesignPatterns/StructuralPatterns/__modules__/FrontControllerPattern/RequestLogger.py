from .abstract import ChainLogger
from .classes import DebugLogger, ErrorLogger, InfoLogger

class RequestLogger():
  __instance = None
  __instantiationBlocker = True

  def __init__(self):
    if RequestLogger.__instantiationBlocker:
      raise Exception(
        "RequestLogger is a Singleton class and an instance can only be "\
        "obtained through getInstance() static method"
      )

    infoLogger  = InfoLogger(ChainLogger.INFO)
    debugLogger = DebugLogger(ChainLogger.DEBUG)
    errorLogger = ErrorLogger(ChainLogger.ERROR)
    debugLogger.setNextLogger(errorLogger)
    infoLogger.setNextLogger(debugLogger)
    self.requestLogger = infoLogger

    RequestLogger.__instance = self
  
  def log(self, level: int, message: str) -> None:
    self.requestLogger.log(level, message)

  @staticmethod
  def getInstance():
    if not RequestLogger.__instance:
      RequestLogger.__instantiationBlocker = False
      RequestLogger()
      RequestLogger.__instantiationBlocker = True
    return RequestLogger.__instance


