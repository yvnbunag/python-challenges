from interface import implements
from .interfaces import ColorFactory, Color
from .classes import Red, Green, Blue

class ColorFactory(implements(ColorFactory)):
  def getColor(self, color: str) -> Color:
    if color.lower() == "red":
      return Red()
    elif color.lower() == "green":
      return Green()
    elif color.lower() == "blue":
      return Blue()
    else:
      return None
    