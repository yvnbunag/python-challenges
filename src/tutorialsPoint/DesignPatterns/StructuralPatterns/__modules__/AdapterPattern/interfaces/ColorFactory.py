from interface import Interface

class ColorFactory(Interface):
  def getColor(self, color: str):
    pass