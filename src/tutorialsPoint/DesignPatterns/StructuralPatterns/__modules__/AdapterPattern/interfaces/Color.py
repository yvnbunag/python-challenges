from interface import Interface

class Color(Interface):
  def getColorRGB(self) -> str:
    pass
  def getColorHex(self) -> str:
    pass