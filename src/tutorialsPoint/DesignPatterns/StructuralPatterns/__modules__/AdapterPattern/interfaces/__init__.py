from .Color import Color
from .ColorFactory import ColorFactory
from .OldColor import OldColor