from interface import Interface

class OldColor(Interface):
  def getOldColorRGB(self) -> str:
    pass
  def getOldColorHex(self) -> str:
    pass