from interface import implements
from ..interfaces import OldColor

class OldBlue(implements(OldColor)):
  def getOldColorRGB(self) -> str:
    return "rgb(0, 0, 255)"

  def getOldColorHex(self) -> str:
    return "#0000FF"