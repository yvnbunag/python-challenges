from interface import implements
from ..interfaces import OldColor

class OldGreen(implements(OldColor)):
  def getOldColorRGB(self) -> str:
    return "rgb(0, 255, 0)"

  def getOldColorHex(self) -> str:
    return "#00FF00"