from interface import implements
from ..interfaces import Color

class Green(implements(Color)):
  def getColorRGB(self) -> str:
    return "rgb(0, 255, 0)"

  def getColorHex(self) -> str:
    return "#00FF00"