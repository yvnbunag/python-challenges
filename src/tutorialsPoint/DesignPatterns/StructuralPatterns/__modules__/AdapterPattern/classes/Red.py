from interface import implements
from ..interfaces import Color

class Red(implements(Color)):
  def getColorRGB(self) -> str:
    return "rgb(255, 0, 0)"

  def getColorHex(self) -> str:
    return "#FF0000"