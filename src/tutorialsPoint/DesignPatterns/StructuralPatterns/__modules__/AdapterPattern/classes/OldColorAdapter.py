from interface import implements
from ..interfaces import Color, OldColor

class OldColorAdapter(implements(Color)):
  def __init__(self, oldColor: OldColor):
    self.__oldColor = oldColor

  def getColorRGB(self) -> str:
    return self.__oldColor.getOldColorRGB()

  def getColorHex(self) -> str:
    return self.__oldColor.getOldColorHex()