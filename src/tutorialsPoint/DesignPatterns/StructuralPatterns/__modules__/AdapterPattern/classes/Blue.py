from interface import implements
from ..interfaces import Color

class Blue(implements(Color)):
  def getColorRGB(self) -> str:
    return "rgb(0, 0, 255)"

  def getColorHex(self) -> str:
    return "#0000FF"