from .Red import Red
from .Green import Green
from .Blue import Blue
from .OldRed import OldRed
from .OldGreen import OldGreen
from .OldBlue import OldBlue
from .OldColorAdapter import OldColorAdapter