from interface import implements
from ..interfaces import OldColor

class OldRed(implements(OldColor)):
  def getOldColorRGB(self) -> str:
    return "rgb(255, 0, 0)"

  def getOldColorHex(self) -> str:
    return "#FF0000"