from interface import implements
from .interfaces import ColorFactory, OldColor
from .classes import OldRed, OldGreen, OldBlue

class OldColorFactory(implements(ColorFactory)):
  def getColor(self, color: str) -> OldColor:
    if color.lower() == "red":
      return OldRed()
    elif color.lower() == "green":
      return OldGreen()
    elif color.lower() == "blue":
      return OldBlue()
    else:
      return None
    