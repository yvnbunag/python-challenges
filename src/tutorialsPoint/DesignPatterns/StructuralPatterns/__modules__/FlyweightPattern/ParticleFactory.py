from .classes import *

class ParticleFactory():
  def __init__(self):
    self.__projectileCollection = {}
  
  def getParticle(self, particleName: str):
    if particleName.lower() in self.__projectileCollection:
      return self.__projectileCollection[particleName.lower()]
    
    if particleName.lower() == "bullet":
      self.__projectileCollection["bullet"] = Bullet()
      return  self.__projectileCollection["bullet"]

    if particleName.lower() == "riflebullet":
      self.__projectileCollection["riflebullet"] = RifleBullet()
      return self.__projectileCollection["riflebullet"]

    if particleName.lower() == "missile":
      self.__projectileCollection["missile"] = Missile()
      return  self.__projectileCollection["missile"]