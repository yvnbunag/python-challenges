from ..abstract import Particle

class Bullet(Particle):
  def __init__(self):
    self.particle = "~"
    self.speed = 3