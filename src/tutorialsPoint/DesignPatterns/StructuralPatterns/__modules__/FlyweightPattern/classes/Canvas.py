class Canvas():
  @staticmethod
  def newCanvas():
    return [" " for a in range(50)]
  
  def display(canvasObject):
    print(f'||{"".join(canvasObject)}||')
