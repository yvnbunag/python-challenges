class Projectile():
  def __init__(self, particle, positionX, direction):
    self.__particle = particle
    self.__positionX = positionX
    self.__direction = direction
  
  def move(self) -> None:
    if self.__direction == "left":
      self.__positionX = self.__particle.moveLeft(self.__positionX)
    elif self.__direction == "right":
      self.__positionX = self.__particle.moveRight(self.__positionX)
  
  def draw(self, canvas) -> None:
    self.__particle.draw(self.__positionX, canvas)

  def checkLife(self, canvas, projectileStore, index) -> None:
    if not (0 < self.__positionX < len(canvas)):
      projectileStore.pop(index)