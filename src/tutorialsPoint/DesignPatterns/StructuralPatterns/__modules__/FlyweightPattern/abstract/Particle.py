from abc import ABC, abstractmethod

class Particle(ABC):
  def __init__(self):
    self.particle = False
    self.speed = False
    if not self.__particle or not self.__speed:
      raise Exception("projectile and speed variables should be overridden")

  def draw(self, xPosition: int, canvas: list) -> None:
    if -1 < xPosition < len(canvas):
      canvas[xPosition] = self.particle

  def moveLeft(self, xPosition: int) -> int:
    return xPosition - self.speed

  def moveRight(self, xPosition) -> int:
    return xPosition + self.speed