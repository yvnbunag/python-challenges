from src.__modules__ import levelPrint
class Employee():
  def __init__(
    self,
    name: str,
    position: str,
    salary: float = 10000.00,
    subordinates: list = []
  ):
    self.__name = name
    self.__position = position
    self.__salary = salary
    self.__subordinates = subordinates
  
  def addSubordinate(self, subordinate) -> None:
    self.__subordinates.append(subordinate)

  def removeSubordinate(self, subordinate) -> None:
    self.__subordinates.remove(subordinate)

  def getSubordinates(self) -> list:
    return self.__subordinates

  def getDetails(self) -> str:
    return {
      "name" : self.__name,
      "position" : self.__position,
      "salary" : self.__salary,
      "noOfSubs" : len(self.__subordinates)
    }

  @staticmethod
  def getHierarchy(rootEmployee, iteration = 0):
    levelPrint(iteration, "---")
    employeeDetails = rootEmployee.getDetails()
    levelPrint(iteration, f'Name: {employeeDetails["name"]}')
    levelPrint(iteration, f'Position: {employeeDetails["position"]}')
    levelPrint(iteration, f'Salary: {employeeDetails["salary"]}')
    if employeeDetails["noOfSubs"]:
      levelPrint(
        iteration, f'No. of Subordinates: {employeeDetails["noOfSubs"]}'
      )
      for employee in rootEmployee.getSubordinates():
        Employee.getHierarchy(employee, iteration + 3)
