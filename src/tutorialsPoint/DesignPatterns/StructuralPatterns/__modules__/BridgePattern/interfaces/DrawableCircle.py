from interface import Interface

class DrawableCircle(Interface):
  def drawCircle(self, x: int, y: int, radius: int) -> None:
    pass