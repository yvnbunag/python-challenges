from ..abstract import Shape
from ..interfaces import DrawableCircle

class Circle(Shape):
  def __init__(self, x: int, y: int, radius: int, drawableCircle: DrawableCircle):
    super().__init__(drawableCircle)
    self.__x = x
    self.__y = y
    self.__radius = radius
  
  def draw(self) -> None:
    self.drawable.drawCircle(self.__x, self.__y, self.__radius)