from interface import implements
from ..interfaces import DrawableCircle

class BlueCircle(implements(DrawableCircle)):
  def drawCircle(self, x: int, y: int, radius: int) -> None:
    print("Blue Circle drawn with details:")
    print("x-coordinate: ", x)
    print("y-coordinate: ", y)
    print("radius: ", radius)