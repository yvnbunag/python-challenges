from abc import ABC, abstractmethod

class Shape(ABC):
  def __init__(self, drawable):
    self.drawable = drawable
  
  @abstractmethod
  def draw(self):
    pass