class OrderAPI():
  def __init__(self):
    self.__storedOrderIDs = [
      "mockOrder001",
      "mockOrder002"
    ]
  
  def getOrder(self, orderID: str) -> bool:
    if orderID in self.__storedOrderIDs:
      return Order(orderID)
    return False

class Order():
  def __init__(self, orderID: str):
    self.__orderID = orderID
  
  def getOrderDetails(self) -> bool:
    return True