from .OrderAPI import Order

class DeliveryAPI():
  def makeDelivery(self, order: Order) -> bool:
    if order.getOrderDetails():
      return {
        "Delivery ID": "mockDeliveryID1",
        "Delivery Address": "mockDeliveryAddress",
        "Estimated Arrival Time": "7 Business Days"
      }
    return False