from .OrderAPI import Order

class InventoryAPI():
  def checkOrderAvailability(self, order: Order) -> bool:
    if order.getOrderDetails():
      return True
    return False