from .classes import *

class OnlineOrderAPI():
  def __init__(self):
    self.__orderAPI = OrderAPI()
    self.__inventoryAPI = InventoryAPI()
    self.__PaymentAPI = PaymentAPI()
    self.__deliveryAPI = DeliveryAPI()

  def process(self, orderID):
    # Create Order object from orderID
    order = self.__orderAPI.getOrder(orderID)
    # Verify order availability
    if not self.__inventoryAPI.checkOrderAvailability(order):
      raise Exception("Order not available")
    if not self.__PaymentAPI.makePayment(order):
      raise Exception("Payment failed")
    deliveryDetails = self.__deliveryAPI.makeDelivery(order)
    if deliveryDetails:
      return deliveryDetails
    raise Exception("Delivery failed")