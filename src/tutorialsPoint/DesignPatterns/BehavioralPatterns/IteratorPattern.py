from src.__modules__ import challenge
from .__modules__.IteratorPattern import Database

class IteratorPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "value"
    self.challengeDescription = "Iterator pattern is very commonly used "\
      "design pattern in Java and .Net programming environment. This pattern "\
      "is used to get a way to access the elements of a collection object in "\
      "sequential manner without any need to know its underlying "\
      "representation."
    self.timeComplexity = ""

  def execute(self) -> None:
    database = Database()
    query = {
      "and": [
        {
          "or": [
            "occupation = engineer",
            "occupation = developer",
            "occupation = accountant",
            "occupation = animator",
            "occupation = nurse"
          ]
        },
        [
          "name like mockName"
        ]
      ]
    }

    rawResults = database.query(query)
    print("Query output in list format looped:")
    for result in rawResults:
      print(result)

    iterableResults = database.queryToIterator(query)
    print("\nQuery output in iterator format looped:")

    while iterableResults.hasNext():
      print(iterableResults.next())

    print("\nIterable has more value?", iterableResults.hasNext())
    print("Iterable Reset")
    iterableResults.reset()
    print("Iterable has more value?", iterableResults.hasNext())
    print(iterableResults)
    print(iterableResults)

    while iterableResults.hasNext():
      print(iterableResults.next())

if __name__ == "__main__":
  main = IteratorPattern()
  main.execute()