from src.__modules__ import challenge
from .__modules__.ModelViewControllerPattern import *

class ModelViewControllerPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/mvc_pattern.htm"
    self.challengeDescription = "MVC Pattern stands for Model-View-Controller"\
      "Pattern. This pattern is used to separate application's concerns.\n"\
      "Model - Model represents an object carrying data. It can also have "\
      "logic to update controller if its data changes.\n"\
      "View - View represents the visualization of the data that model "\
      "contains.\n"\
      "Controller - Controller acts on both model and view. It controls the "\
      "data flow into model object and updates the view whenever data "\
      "changes. It keeps view and model separate."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    clientDatabase = ClientDatabase()

    client01Controller = ClientController(
      clientDatabase.get({"id": "MCKID01"}),
      ClientView.getInstance()
    )
    client02Controller = ClientController(
      clientDatabase.get({"id": "MCKID02"}),
      ClientView.getInstance()
    )

    print("*Controller displaying client object in default view:")
    client01Controller.printDefaultView()
    client01Controller.setFirstName("ManuallySetFN01")
    print("\n*Controller setting first name to another value:")
    client01Controller.printDefaultView()

    print("\n*Model object in dictionary format:")
    print(client02Controller.getDictionaryFormat())

if __name__ == "__main__":
  main = ModelViewControllerPattern()
  main.execute()