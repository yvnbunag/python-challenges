from .classes import DatabaseRequestIterator
from .abstract import Iterator

class Database():
  def __init__(self):
    self.queryResults = [
      {
        "name": "mockNameA m. mockSurnameA",
        "age": 25,
        "gender": "m",
        "occupation": "engineer"
      },
      {
        "name": "mockNameB m. mockSurnameB",
        "age": 21,
        "gender": "f",
        "occupation": "developer"
      },
      {
        "name": "mockNameC m. mockSurnameC",
        "age": 28,
        "gender": "f",
        "occupation": "accountant"
      },
      {
        "name": "mockNameD m. mockSurnameD",
        "age": 31,
        "gender": "m",
        "occupation": "animator"
      },
      {
        "name": "mockNameE m. mockSurnameE",
        "age": 23,
        "gender": "f",
        "occupation": "nurse"
      }
    ]
  
  def query(self, query:dict = {}) -> list:
    return self.queryResults

  def queryToIterator(self, query:dict = {}) -> Iterator:
    return DatabaseRequestIterator(self.queryResults)
