from abc import ABC, abstractmethod

class Iterator(ABC):
  def __init__(self, iterable: list = []):
    self.iterable = iterable
    self.index = -1
  
  def __repr__(self):
    if self.index < 0:
      self.index = 0
    return self.iterable[self.index]
  
  def __str__(self):
    if self.index < 0:
      self.index = 0
    return str(self.iterable[self.index])

  def hasNext(self) -> bool:
    if (self.index + 1) < len(self.iterable):
      return True
    return False
  
  def next(self):
    self.index += 1
    return self
      
  def reset(self):
    self.index = -1
    return self