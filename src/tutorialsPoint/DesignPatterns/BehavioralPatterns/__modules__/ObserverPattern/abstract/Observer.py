from abc import ABC, abstractmethod
from ..classes import Subject

class Observer(ABC):
  def __init__(self, subject: Subject):
    if subject:
      self.attachTo(subject)

  def update(self) -> None:
    self.state = self.subject.getState()

  def attachTo(self, subject: Subject) -> None:
    self.subject = subject
    self.subject.attach(self)
    self.state = self.subject.getState()

  def detach(self) -> None:
    self.subject.detachObserver(self)
    self.subject = None

  @abstractmethod
  def __repr__(self):
    pass
  
  @abstractmethod
  def __str__(self):
    pass
  
  
