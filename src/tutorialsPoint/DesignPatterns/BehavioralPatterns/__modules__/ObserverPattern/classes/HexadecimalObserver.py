from ..abstract import Observer

class HexadecimalObserver(Observer):
  def __repr__(self) -> str:
    return str(hex(self.state))[2:].upper()
  
  def __str__(self) -> str:
    return str(hex(self.state))[2:].upper()