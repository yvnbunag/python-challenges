from ..abstract import Observer

class Subject():
  def __init__(self , state = None):
    self.__observers = []
    self.__state = state

  def __repr__(self):
    return self.__state
  
  def __str__(self):
    return str(self.__state)

  def getState(self):
    return self.__state

  def setState(self, state):
    self.__state = state
    self.notifyObservers()

  def attach(self, observer: Observer):
    self.__observers.append(observer)

  def detachObserver(self, observer: Observer):
    self.__observers.remove(observer)

  def notifyObservers(self):
    for observer in self.__observers:
      observer.update()