from ..abstract import Observer

class OctalObserver(Observer):
  def __repr__(self) -> str:
    return str(oct(self.state))[2:]
  
  def __str__(self) -> str:
    return str(oct(self.state))[2:]