from ..abstract import Observer

class BinaryObserver(Observer):
  def __repr__(self) -> str:
    return str(bin(self.state))[2:]
  
  def __str__(self) -> str:
    return str(bin(self.state))[2:]