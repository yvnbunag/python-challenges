from interface import implements
from ..interfaces import ArithmeticStrategy

class Add(implements(ArithmeticStrategy)):
  def evaluate(self, num1: int, num2: int) -> int:
    return num1 + num2