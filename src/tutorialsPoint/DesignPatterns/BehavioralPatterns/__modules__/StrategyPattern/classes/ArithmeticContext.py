from ..interfaces import ArithmeticStrategy

class ArithmeticContext():
  def __init__(self, strategy: ArithmeticStrategy = None):
    self.__strategy = strategy

  def evaluate(self, num1: int, num2: int) -> int:
    return self.__strategy.evaluate(num1, num2)

  def setStrategy(self, strategy: ArithmeticStrategy) -> None:
    self.__strategy = strategy