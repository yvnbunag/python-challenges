from interface import Interface

class ArithmeticStrategy(Interface):
  def evaluate(self, num1: int, num2: int) -> int:
    pass