from interface import implements
from ..interfaces import Element

class InputElement(implements(Element)):
  def getElement(self) -> str:
    return f'Input@{hex(id(self))}'