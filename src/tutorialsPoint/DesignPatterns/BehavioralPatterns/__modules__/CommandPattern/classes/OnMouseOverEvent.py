from ..abstract import Event
from ..interfaces import Element

class OnMouseOverEvent(Event):
  def execute(self) -> None:
    print(f'{self.element.getElement()} element is being hovered')
    # perform element actions here