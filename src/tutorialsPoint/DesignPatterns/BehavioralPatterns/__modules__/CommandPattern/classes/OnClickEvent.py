from ..abstract import Event
from ..interfaces import Element

class OnClickEvent(Event):
  def execute(self) -> None:
    print(f'{self.element.getElement()} element clicked')
    # perform element actions here