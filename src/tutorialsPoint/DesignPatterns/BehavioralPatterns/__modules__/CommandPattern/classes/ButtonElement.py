from interface import implements
from ..interfaces import Element

class ButtonElement(implements(Element)):
  def getElement(self) -> str:
    return f'Button@{hex(id(self))}'