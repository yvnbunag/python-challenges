from ..abstract import Event
from ..interfaces import Element

class OnMouseOutEvent(Event):
  def execute(self) -> None:
    print(f'{self.element.getElement()} element has been hovered out')
    # perform element actions here