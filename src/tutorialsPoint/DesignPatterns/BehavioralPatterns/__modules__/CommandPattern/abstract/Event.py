from abc import ABC, abstractmethod
from ..interfaces import Element

class Event(ABC):
  def __init__(self, element: Element):
    self.element = element

  @abstractmethod
  def execute(self) -> None:
    pass