from .abstract import Event
class EventManager():
  def manage(self, event: Event):
    event.execute()
  