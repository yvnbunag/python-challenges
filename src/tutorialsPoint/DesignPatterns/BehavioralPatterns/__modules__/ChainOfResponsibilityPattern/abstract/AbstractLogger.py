from abc import ABC, abstractmethod

class AbstractLogger(ABC):
  ERROR = 3
  DEBUG = 2
  INFO = 1

  def __init__(self, level):
    self.nextLogger = None
    self.level = level

  def setNextLogger(self, nextLogger):
    self.nextLogger = nextLogger
    return self

  def logMessage(self, level: int, message: str) -> None:
    if self.level <= level:
      self.logOperation(message)
    if self.nextLogger:
      self.nextLogger.logMessage(level, message)

  @abstractmethod
  def logOperation(self, message) -> None:
    pass