from .abstract import AbstractLogger
from .classes import DebugLogger, ErrorLogger, InfoLogger

class LoggerChain():
  __instance = None
  __instantiationBlock = True
  def __init__(self):
    if LoggerChain.__instantiationBlock:
      raise Exception(
        "LoggerChain class implements singleton pattern and can only be "\
        "Instantiated through getInstance() method"
      )

    self.errorLogger = ErrorLogger(AbstractLogger.ERROR)
    self.debugLogger = DebugLogger(AbstractLogger.DEBUG) \
      .setNextLogger(self.errorLogger)
    self.infoLogger = InfoLogger(AbstractLogger.INFO) \
      .setNextLogger(self.debugLogger)
    self.loggerChain = self.infoLogger

    LoggerChain.__instance = self
  
  def log(self, level, message) -> None:
    self.loggerChain.logMessage(level, message)

  @staticmethod
  def getInstance():
    if not LoggerChain.__instance:
      LoggerChain.__instantiationBlock = False
      LoggerChain()
      LoggerChain.__instantiationBlock = True
    return LoggerChain.__instance

