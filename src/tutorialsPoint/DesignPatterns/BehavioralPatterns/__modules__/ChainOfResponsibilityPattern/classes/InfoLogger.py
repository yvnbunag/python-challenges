from ..abstract import AbstractLogger

class InfoLogger(AbstractLogger):
  def logOperation(self, message) -> None:
    print("INFO LOG:", message)