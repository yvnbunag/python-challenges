from ..abstract import AbstractLogger

class DebugLogger(AbstractLogger):
  def logOperation(self, message) -> None:
    print("DEBUG LOG:", message)