from ..abstract import AbstractLogger

class ErrorLogger(AbstractLogger):
  def logOperation(self, message) -> None:
    print("ERROR LOG:", message)