class ClientModel():
  def __init__(
    self,
    id: str,
    firstName: str,
    lastName: str,
    age: int,
    gender: str
  ):
    self.__id = id
    self.__firstName = firstName
    self.__lastName = lastName
    self.__age = age
    self.__gender = gender

  def getID(self) -> str:
    return self.__id

  def setID(self, value: str) -> None:
    self.__id = value

  def getFirstName(self) -> str:
    return self.__firstName

  def setFirstName(self, value: str) -> None:
    self.__firstName = value

  def getLastName(self) -> str:
    return self.__lastName

  def setLastName(self, value: str) -> None:
    self.__lastName = value

  def getAge(self) -> int:
    return self.__age

  def setAge(self, value: int) -> None:
    self.__age = value

  def getGender(self) -> str:
    return self.__gender

  def setGender(self, value: str) -> None:
    self.__gender = value