from . import ClientModel

class ClientDatabase():
  def get(self, query: dict):
    if query == {"id": "MCKID01"}:
      return ClientModel(
        "MCKID01",
        "MockFN01",
        "MockLN01",
        27,
        "M"
      )
    elif query == {"id": "MCKID02"}:
      return ClientModel(
        "MCKID02",
        "MockFN02",
        "MockLN02",
        32,
        "F"
      )

    return None