from . import ClientModel, ClientView

class ClientController():
  def __init__(self, clientModel: ClientModel, clientView: ClientView):
    self.__clientModel = clientModel
    self.__clientView = clientView

  def getID(self) -> str:
    return self.__clientModel.getID()

  def setID(self, value: str) -> None:
    self.__clientModel.setID(value)

  def getFirstName(self) -> str:
    return self.__clientModel.getFirstName()

  def setFirstName(self, value: str) -> None:
    self.__clientModel.setFirstName(value)

  def getLastName(self) -> str:
    return self.__clientModel.getLastName()

  def setLastName(self, value: str) -> None:
    self.__clientModel.setLastName(value)

  def getAge(self) -> int:
    return self.__clientModel.getAge()

  def setAge(self, value: int) -> None:
    self.__clientModel.setAge(value)

  def getGender(self) -> str:
    return self.__clientModel.getGender()

  def setGender(self, value: str) -> None:
    self.__clientModel.setGender(value)

  def printDefaultView(self):
    self.__clientView.printDefaultView(self.__clientModel)

  def getDictionaryFormat(self) -> dict:
    return self.__clientView.getDictionaryFormat(self.__clientModel)