from . import ClientModel

class ClientView():
  __instance =  None
  __instantiationBlock = True

  def __init__(self):
    if ClientView.__instantiationBlock:
      raise Exception(
        "ClientView is a singleton class and the instance can only be "\
        "obtained through getInstance() method"
      )
    ClientView.__instance = self

  def printDefaultView(self, clientModel: ClientModel) -> None:
    print("Client Name:", clientModel.getFirstName(), clientModel.getLastName())
    print("Client ID:", clientModel.getID())
    print("Client Age:", clientModel.getAge())
    print("Client Gender:", "Male" if clientModel.getGender() == "M" else "Female")

  def getDictionaryFormat(self, clientModel: ClientModel) -> dict:
    return {
      "firstName": clientModel.getFirstName(),
      "lastName": clientModel.getLastName(),
      "id": clientModel.getID(),
      "age": clientModel.getAge(),
      "gender": clientModel.getGender()
    }

  @staticmethod
  def getInstance():
    if not ClientView.__instance:
      ClientView.__instantiationBlock = False
      ClientView()
      ClientView.__instantiationBlock = True
    return ClientView.__instance
