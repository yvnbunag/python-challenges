from abc import ABC, abstractmethod

class Image(ABC):
  def __init__(self, imageName: str = None, imageData: str = None):
    self.imageName = imageName
    self.imageData = imageData

  @abstractmethod
  def getImage(self) -> str:
    pass

  @abstractmethod
  def isNil(self) -> bool:
    pass

  def getImageName(self) -> str:
    return self.imageName
