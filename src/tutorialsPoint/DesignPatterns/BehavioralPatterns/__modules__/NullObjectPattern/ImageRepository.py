from .abstract import Image
from .classes import *


class ImageRepository():
  def __init__(self):
    self.__images = [
      "mockImg01",
      "mockImg03",
      "mockImg05"
    ]

    # Template for mock image data from external repository
    self.__imageDataTemplate = ""\
      f'{"*"*20}\n'\
      f'*{" "*18}*\n'\
      f'*{" "*18}*\n'\
      f'*{" "*5}{"X"*9}{" "*4}*\n'\
      f'*{" "*7}image{" "*6}*\n'\
      f'*{" "*5}displayed{" "*4}*\n'\
      f'*{" "*18}*\n'\
      f'*{" "*18}*\n'\
      f'{"*"*20}'
    
  def get(self, imageName) -> Image:
    if imageName in self.__images:
      return RealImage(
        imageName,
        self.__imageDataTemplate.replace("X"*9, imageName)
      )
    
    return NullImage(imageName)