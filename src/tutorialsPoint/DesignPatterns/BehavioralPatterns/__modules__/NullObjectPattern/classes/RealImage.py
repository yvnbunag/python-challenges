from ..abstract import Image

class RealImage(Image):
  def getImage(self) -> str:
    return self.imageData

  def isNil(self) -> bool:
    return False