from ..abstract import Image

class NullImage(Image):
  def getImage(self) -> str:
    return ""\
      f'{"*"*20}\n'\
      f'*{" "*18}*\n'\
      f'*{" "*18}*\n'\
      f'*{" "*5}{self.imageName}{" "*4}*\n'\
      f'*{" "*5}not found{" "*4}*\n'\
      f'*{" "*5}on server{" "*4}*\n'\
      f'*{" "*18}*\n'\
      f'*{" "*18}*\n'\
      f'{"*"*20}'

  def isNil(self) -> bool:
    return True