from src.__modules__ import challenge
from .__modules__.NullObjectPattern import ImageRepository

class NullObjectPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/null_object_pattern.htm"
    self.challengeDescription = "In Null Object pattern, a null object "\
      "replaces check of NULL object instance. Instead of putting if check "\
      "for a null value, Null Object reflects a do nothing relationship. Such "\
      "Null object can also be used to provide default behaviour in case data "\
      "is not available. In Null Object pattern, we create an abstract class "\
      "specifying various operations to be done, concrete classes extending "\
      "this class and a null object class providing do nothing implemention "\
      "of this class and will be used seemlessly where we need to check null "\
      "value."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    imageRepository = ImageRepository()
    imageFetch = [
      "mockImg01",
      "mockImg02",
      "mockImg03",
      "mockImg04",
      "mockImg05"
    ]
    
    for imageName in imageFetch:
      image = imageRepository.get(imageName)
      print(
        image.getImageName(),
        "image:",
        "Null object" if image.isNil() else "Real object"
      )
      print(image.getImage(), end = "\n\n")

if __name__ == "__main__":
  main = NullObjectPattern()
  main.execute()