from src.__modules__ import challenge
from .__modules__.CommandPattern import EventManager
from .__modules__.CommandPattern.classes import *

class CommandPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/command_pattern.htm"
    self.challengeDescription = "Command pattern is a data driven design "\
      "pattern and falls under behavioral pattern category. A request is "\
      "wrapped under an object as command and passed to invoker object. "\
      "Invoker object looks for the appropriate object which can handle this "\
      "command and passes the command to the corresponding object which executes the command."
    self.timeComplexity = "O(1) - Constant Time"

  def execute(self) -> None:
    buttonA = ButtonElement()
    buttonB = ButtonElement()
    inputA = InputElement()
    inputB = InputElement()

    eventSet = [
      OnMouseOverEvent(buttonA),
      OnClickEvent(buttonA),
      OnMouseOutEvent(buttonA),
      OnMouseOverEvent(inputA),
      OnMouseOutEvent(inputA),
      OnMouseOverEvent(buttonB),
      OnMouseOutEvent(buttonB)
    ]

    eventManager = EventManager()
    
    for event in eventSet:
      eventManager.manage(event)

    commandA = OnMouseOverEvent(inputB)
    commandB = OnClickEvent(inputB)
    commandC = OnMouseOutEvent(inputB)

    eventManager.manage(commandA)
    eventManager.manage(commandB)
    eventManager.manage(commandC)
    

if __name__ == "__main__":
  main = CommandPattern()
  main.execute()