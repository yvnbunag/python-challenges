from src.__modules__ import challenge
from .__modules__.ChainOfResponsibilityPattern import LoggerChain
from .__modules__.ChainOfResponsibilityPattern.abstract import AbstractLogger

class ChainOfResponsibilityPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/chain_of_responsibility_pattern.htm"
    self.challengeDescription = "As the name suggests, the chain of "\
      "responsibility pattern creates a chain of receiver objects for a "\
      "request. This pattern decouples sender and receiver of a request based "\
      "on type of request. This pattern comes under behavioral patterns."\
      "In this pattern, normally each receiver contains reference to another "\
      "receiver. If one object cannot handle the request then it passes the "\
      "same to the next receiver and so on."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    loggerChain = LoggerChain.getInstance()
    loggerChain.log(AbstractLogger.INFO, "Info level message")
    print()
    loggerChain.log(AbstractLogger.DEBUG, "Debug level message")
    print()
    loggerChain.log(AbstractLogger.ERROR, "Error level message")

if __name__ == "__main__":
  main = ChainOfResponsibilityPattern()
  main.execute()