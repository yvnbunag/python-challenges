from src.__modules__ import challenge
from .__modules__.ObserverPattern.classes import *

class ObserverPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/observer_pattern.htm"
    self.challengeDescription = "Observer pattern is used when there is "\
      "one-to-many relationship between objects such as if one object is "\
      "modified, its depenedent objects are to be notified automatically. "\
      "Observer pattern falls under behavioral pattern category."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    subject = Subject()
    binaryObserver = BinaryObserver(subject)
    octalObserver = OctalObserver(subject)
    hexadecimalObserver = HexadecimalObserver(subject)
    
    states = [15, 16, 31, 32]

    for state in states:
      subject.setState(state)
      print("Subject value:", subject)
      print("Binary observer value:", binaryObserver)
      print("Octal observer value:", octalObserver)
      print("Hexadecimal observer value:", hexadecimalObserver)
      print()

    hexadecimalObserver2 = HexadecimalObserver(subject)
    print("Newly attached hexadecimal observer value:", hexadecimalObserver2)
    hexadecimalObserver2.detach()
    subject.setState(47)
    print("New subject value:", subject)
    print("Detached hexadecimal observer value:", hexadecimalObserver2)
    hexadecimalObserver2.attachTo(subject)
    print("Reattached hexadecimal observer value:", hexadecimalObserver2)

if __name__ == "__main__":
  main = ObserverPattern()
  main.execute()