from src.__modules__ import challenge
from .__modules__.StrategyPattern.classes import *

class StrategyPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/strategy_pattern.htm"
    self.challengeDescription = "In Strategy pattern, a class behavior or its "\
      "algorithm can be changed at run time. This type of design pattern "\
      "comes under behavior pattern. In Strategy pattern, we create objects "\
      "which represent various strategies and a context object whose behavior "\
      "varies as per its strategy object. The strategy object changes the "\
      "executing algorithm of the context object."
    self.timeComplexity = "O(n^2) - Quadratic Time"

  def execute(self) -> None:
    print("<<Simple evaluation>>")
    num1 = 20
    num2 = 5
    arithmeticContext = ArithmeticContext(Add())
    print(f'{num1} + {num2} =', arithmeticContext.evaluate(num1, num2))
    arithmeticContext = ArithmeticContext(Subtract())
    print(f'{num1} - {num2} =', arithmeticContext.evaluate(num1, num2))
    arithmeticContext = ArithmeticContext(Multiply())
    print(f'{num1} * {num2} =', arithmeticContext.evaluate(num1, num2))
    arithmeticContext = ArithmeticContext(Divide())
    print(f'{num1} / {num2} =', arithmeticContext.evaluate(num1, num2))

    print("\n<<Looped evaluation>>")

    numSets = [
      [10, 2],
      [30, 15]
    ]
    strategySet = {
      "+": Add(),
      "-": Subtract(),
      "*": Multiply(),
      "/": Divide()
    }
    reusableContext = ArithmeticContext()

    for numSet in numSets:
      num1 = numSet[0]
      num2 = numSet[1]
     
      for operation, strategy in strategySet.items():
        reusableContext.setStrategy(strategy)
        print(
          f'{num1} {operation} {num2} =',
          reusableContext.evaluate(num1, num2)
        )
      print()


if __name__ == "__main__":
  main = StrategyPattern()
  main.execute()