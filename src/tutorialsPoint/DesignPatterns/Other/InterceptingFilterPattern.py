from src.__modules__ import challenge
from .__modules__.InterceptingFilterPattern.classes import User
from .__modules__.InterceptingFilterPattern import Dispatcher
class InterceptingFilterPattern(challenge):
  def __init__(self):
    super().__init__()
    self.challengeURL = "https://www.tutorialspoint.com/design_pattern/intercepting_filter_pattern.htm"
    self.challengeDescription = "The intercepting filter design pattern is "\
      "used when we want to do some pre-processing / post-processing with "\
      "request or response of the application. Filters are defined and "\
      "applied on the request before passing the request to actual target "\
      "application. Filters can do the authentication/ authorization/ logging "\
      "or tracking of request and then pass the requests to corresponding "\
      "handlers. Following are the entities of this type of design pattern."\
      " \nFilter - Filter which will performs certain task prior or after "\
      "execution of request by request handler."\
      " \nFilter Chain - Filter Chain carries multiple filters and help to "\
      "execute them in defined order on target."\
      " \nTarget - Target object is the request handler"\
      " \nFilter Manager - Filter Manager manages the filters and Filter "\
      "Chain."\
      " \nClient - Client is the object who sends request to the Target object."
    self.timeComplexity = "O(n) - Linear Time"

  def execute(self) -> None:
    silverAdminUser = User(
      "Silver Admin",
      "admin",
      ["silverSubscription"]
    )

    goldNonAdminUser = User(
      "Gold Regular",
      "regular",
      ["goldSubscription"]
    )

    goldAdminUser = User(
      "Gold Admin",
      "admin",
      ["goldSubscription"]
    )

    requests = [
      {"/silverExclusive": silverAdminUser},
      {"/users": goldNonAdminUser},
      {"/goldExclusive": silverAdminUser},
      {"/goldExclusive": goldNonAdminUser},
      {"/users": silverAdminUser},
      {"/goldExclusive": goldAdminUser},
    ]

    dispatcher = Dispatcher()
    for request in requests:
      URL, USER = next(iter(request.items()))
      print(
        f'\nINCOMING REQUEST:: {URL} access request from User = {USER.getName()}'
      )
      response = dispatcher.dispatchRequest(URL, USER)
      print(f'OUTGOING RESPONSE:: {response}')


if __name__ == "__main__":
  main = InterceptingFilterPattern()
  main.execute()