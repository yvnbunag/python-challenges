from .FilterChain import FilterChain
from .classes import User
from .interfaces import Filter, FilteredServices

class FilterManager():
  def __init__(self, target: FilteredServices = None):
    self.__filterChain = FilterChain(target)

  def addFilter(self, filter: Filter) -> None:
    self.__filterChain.addFilter(filter)

  def setTarget(self, target: FilteredServices):
    self.__filterChain.setTarget(target)

  def execute(self, user: User):
    return self.__filterChain.execute(user)