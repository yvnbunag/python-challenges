from .interfaces import *
from .classes import User
class FilterChain():
  def __init__(self, target: FilteredServices = None):
    self.__filters = []
    self.__target = target

  def addFilter(self, filter: Filter) -> None:
    self.__filters.append(filter)

  def setTarget(self, target: FilteredServices):
    self.__target = target

  def execute(self, user: User):
    try:
      for filterObject in self.__filters:
        filterObject.filter(user)
      if self.__target:
        return self.__target.execute(user)
        
      return {
        "success": False,
        "reason": "No service provided"
      }
      
    except Exception as err:
      return {
        "success": False,
        "reason": str(err),
        "requester": user.getName()
      }

