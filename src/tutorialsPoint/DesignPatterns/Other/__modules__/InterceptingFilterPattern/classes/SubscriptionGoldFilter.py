from interface import implements
from ..interfaces import Filter
from ..classes import User

class SubscriptionGoldFilter(implements(Filter)):
  def filter(self, user) -> None:
    if not user.hasSubscription("goldSubscription"):
      raise Exception("User does not have gold subscription")