from interface import implements
from ..interfaces import Filter

class SubscriptionSilverFilter(implements(Filter)):
  def filter(self, user) -> None:
    if not user.hasSubscription("silverSubscription"):
      raise Exception("User does not have silver subscription")