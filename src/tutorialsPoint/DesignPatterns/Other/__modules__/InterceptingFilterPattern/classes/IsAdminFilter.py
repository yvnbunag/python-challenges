from interface import implements
from ..interfaces import Filter
class IsAdminFilter(implements(Filter)):
  def filter(self, user) -> None:
    if user.getType().lower() != "admin":
      raise Exception("User does not have admin credentials")