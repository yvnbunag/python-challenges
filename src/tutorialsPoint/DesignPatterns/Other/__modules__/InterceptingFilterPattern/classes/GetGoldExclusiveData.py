from interface import implements
from ..interfaces import FilteredServices

class GetGoldExclusiveData(implements(FilteredServices)):
  def execute(self, user) -> dict:
    return {
      "success": True,
      "data": "GoldExclusiveData",
      "requester": user.getName()
    }