class User():
  def __init__(self, name: str, userType: str, subscriptions: list):
    self.__name = name
    self.__type = userType
    self.__subscriptions = subscriptions
  
  def getName(self) -> str:
    return self.__name

  def getType(self) -> str:
    return self.__type

  def getSubscriptions(self) -> list:
    return self.__subscriptions

  def hasSubscription(self, subscription: str) -> bool:
    return subscription in self.__subscriptions