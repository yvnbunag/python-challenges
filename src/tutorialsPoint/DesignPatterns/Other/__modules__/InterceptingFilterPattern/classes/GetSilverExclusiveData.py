from interface import implements
from ..interfaces import FilteredServices

class GetSilverExclusiveData(implements(FilteredServices)):
  def execute(self, user) -> dict:
    return {
      "success": True,
      "data": "SilverExclusiveData",
      "requester": user.getName()
    }