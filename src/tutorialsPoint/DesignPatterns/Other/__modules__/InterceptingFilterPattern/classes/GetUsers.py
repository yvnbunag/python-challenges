from interface import implements
from ..interfaces import FilteredServices

class GetUsers(implements(FilteredServices)):
  def execute(self, user) -> dict:
    return {
      "success": True,
      "data": "UsersData",
      "requester": user.getName()
    }