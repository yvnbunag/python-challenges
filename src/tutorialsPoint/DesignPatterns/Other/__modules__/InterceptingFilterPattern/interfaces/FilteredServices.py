from interface import Interface

class FilteredServices(Interface):
  def execute(self, user) -> dict:
    pass