from .classes import *
from .FilterManager import FilterManager

class Dispatcher():
  def dispatchRequest(self, URL: str, user: User):
    filterManager = FilterManager()
    # Controller shortcut
    if URL == "/users":
      filterManager.addFilter(IsAdminFilter())
      filterManager.setTarget(GetUsers())
      return filterManager.execute(user)
    elif URL == "/silverExclusive":
      filterManager.addFilter(SubscriptionSilverFilter())
      filterManager.setTarget(GetSilverExclusiveData())    
      return filterManager.execute(user)  
    elif URL == "/goldExclusive":
      filterManager.addFilter(IsAdminFilter())
      filterManager.addFilter(SubscriptionGoldFilter())
      filterManager.setTarget(GetGoldExclusiveData())    
      return filterManager.execute(user)
    else:
      return {
        "success": False,
        "reason": "Service does not exist"
      }
