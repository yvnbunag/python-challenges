# python-challenges

Collection of various coding challenge answers, exploring python basics,
data structures, regular expressions and design patterns.

## Installation
Set up python(3) virtual environment and install dependencies
```sh
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Running the program

Source virtual environment
###
```sh
source venv/bin/activate
```

Run script
```sh
python main.py
```
