#main.py

import os
from typing import Union
from importlib import import_module
from src.__modules__ import  levelPrint, customString as cs
from colorama import init as coloramaInit

class Main:
  def __init__(self):
    """
      Instantiate the base directory of the challenges and maps its content into
      a dictionary
    """
    coloramaInit(autoreset=True)
    self.challengeBase = "src"
    self.challengeMap = {}
    self.currentDirectory = ".root"
    srcList = [x for x in os.listdir(self.challengeBase) if x != "__modules__"]

    for srcFD in srcList:
      currentDirectory = f'{self.challengeBase}/{srcFD}'
      self.challengeMap[srcFD] = self.findExecutable(currentDirectory)
  
  def findExecutable(self, directory: int) -> dict:
    """
      Iterates over all directories and executable files in the challenges
      directory, storing them into a dictionary
    """
    cDir = "__pycache__" 
    mDir = "__modules__" 
    directoryList = [x for x in os.listdir(directory) if x != cDir and x != mDir]
    if "__init__.py" not in directoryList:
      directoryMap = {}

      for directoryFD in directoryList:
        currentDirectory = f'{directory}/{directoryFD}'
        directoryMap[directoryFD] = self.findExecutable(currentDirectory)
      return directoryMap
    else:
      directoryList.remove("__init__.py")
      return directoryList

  def start(self) -> None:
    """ Starts the python program """
    print("Welcome to the collection of Python challenges")
    levelPrint(1, "Directory options are color coded based on its type:")
    levelPrint(2, cs.colored("Directory: Green", "directory"))
    levelPrint(2, cs.colored("Challenge: Yellow", "challenge"))
    self.rootNavigateChallenges()

  def rootNavigateChallenges(self) -> None:
    """ Sets the directory navigation properties to root directory settings"""
    self.currentDirectory = ".root"
    self.navigateChallenges(self.challengeMap)
  
  def navigateChallenges(self, challengeMap: Union[dict, list]) -> None:
    """
      Main method in navigating challenge directories and selecting challenges
      to be run
    """
    def navigationSelected(selection: int, head: Union[dict, list]) -> None:
      """
        Evaluates user selection to either display next directory contents or
        run a challenge
      """
      selectedKey = headKeys[selection-1]
      if isinstance(head, list):
        self.executeChallenge(selectedKey)
        self.navigateChallenges(head)
      else:
        nextKey = selectedKey
        nextHead = head[nextKey]
        self.currentDirectory += f'/{nextKey}'
        self.navigateChallenges(nextHead)

    def backHead() -> None:
      """ Returns the directory view up by one level """
      cdList = self.currentDirectory.split('/')
      backHead = dict(self.challengeMap)
      self.currentDirectory = ".root"

      for key in [len(cdList) - 1, 0]:
        cdList.pop(key)
      for directory in cdList:
          backHead = backHead[directory]
          self.currentDirectory += f'/{directory}'
      self.navigateChallenges(backHead)

    def backToStart() -> None:
      """ Returns the directory view to the root """
      self.rootNavigateChallenges()

    def programStopped() -> None:
      """
        Prints a message that signifies that the program recursion has ended
      """
      print("Python challenges program stopped")
    
    if isinstance(challengeMap, dict): 
      head = dict(challengeMap)
    elif isinstance(challengeMap, list):
      head = list(challengeMap)
    headKeys = []

    levelPrint(1, "Navigate thru the challenges directory:")
    levelPrint(2, cs.colored(f'{self.currentDirectory}/', 'dirList'))

    ctr = 0
    
    if isinstance(head, dict):
      for k,v in head.items():
        headKeys.append(k)
        ctr+=1
        levelPrint(3, cs.colored(f'({ctr}) {cs.formatDir(k)}', 'directory'))
    else:
      for v in head:
        headKeys.append(v)
        ctr+=1
        levelPrint(3, cs.colored(f'({ctr}) {cs.formatChal(v)}', 'challenge'))
    
    if self.currentDirectory != ".root":
      ctr+=1
      levelPrint(3, cs.colored(f'({ctr}) Back', 'back'))
      ctr+=1
      levelPrint(3, cs.colored(f'({ctr}) Back to root', 'start'))

    ctr+=1
    levelPrint(3, cs.colored(f'({ctr}) Exit', 'exit'))

    try:
      selection =int(input(f'\nEnter choice (1-{ctr}): '))
      if self.currentDirectory == ".root":
        if 1 <= selection <= ctr-1:
          navigationSelected(selection, head)
        elif 1 <= selection <= ctr:
          programStopped()
        else:
          raise ValueError("Selection not within given range of values")
      else:
        if 1 <= selection <= ctr-3:
          navigationSelected(selection, head)
        elif 1 <= selection <= ctr-2:
          backHead()
        elif 1 <= selection <= ctr-1:
          backToStart()
        elif 1 <= selection <= ctr:
          programStopped()
        else:
          raise ValueError("Selection not within given range of values")
    except ValueError as err:
      print(cs.colored(f'\n{err}\n', "error"))
      self.navigateChallenges(head)

  def executeChallenge(self, selection: str) -> None:
    """ Runs the challenge selected by the user """
    def repeatPrompt() -> None:
      """ Prompts the user if they would like to repeat the challenge """
      try:
        repeat = input("Repeat challenge? (y/n): ")
        if repeat.lower() == 'y':
          self.executeChallenge(selection)
        elif repeat.lower() == 'n':
          challengeName = cs.formatChal(selection)
          print(
            cs.colored(f'\n{challengeName} challenge closed\n', "finished")
          )
        else:
          raise ValueError("Selection not within the options provided")
      except ValueError as err:
        print(cs.colored(f'\n{err}\n', "error"))
        repeatPrompt()

    temp = self.currentDirectory
    directory = f'{self.challengeBase}.{temp[6:].replace("/",".")}'
    challengeModule = getattr(
      import_module(directory), selection[0:-3]
    )
    challenge = challengeModule()

    print(f'\n{challenge.getTitle()}')
    print(f'>> {challenge.getURL()}')
    print(f'- {challenge.getDescription()}')
    print(f'- Time Complexity: {challenge.getTimeComplexity()}\n')
    challenge.execute()
    print()
    challenge = None
    repeatPrompt()

if __name__ == "__main__":
  main = Main()
  main.start()